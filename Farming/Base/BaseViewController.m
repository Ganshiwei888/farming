//
//  SHBBaseViewController.m
//  SaleTool
//
//  Created by BOX on 16/10/10.
//  Copyright © 2016年 rongkai. All rights reserved.
//

#import "BaseViewController.h"
#import <RESideMenu.h>

@interface BaseViewController ()


@property (strong, nonatomic) UIButton *leftButton;
@property (strong, nonatomic) UIButton *rightButton;

@end

@implementation BaseViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupNavigationItem];
#ifdef __IPHONE_11_0

#endif
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *className = NSStringFromClass([self class]);
    if ([className isEqualToString:@"FarmingViewController"]) {
        self.sideMenuViewController.panGestureEnabled = YES;
    } else {
        self.sideMenuViewController.panGestureEnabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    DLog(@"%@didReceiveMemoryWarning",NSStringFromClass([self class]));
}

- (void)dealloc{
    DLog(@"%@销毁了，内存得到释放",NSStringFromClass([self class]));
}

#pragma mark - Init
- (void)setupNavigationItem{
    if (self.navigationController.viewControllers.count >1) {
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"return"] style:UIBarButtonItemStyleDone target:self action:@selector(backAction)];
        self.navigationItem.leftBarButtonItem = leftItem;
    }
}

#pragma mark - ButtonAction
- (void)backAction{
//    if (self.presentingViewController) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    } else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBarButtonItemAction{

}

- (void)leftBarButtonItemAction{

}

#pragma mark - Setter

- (void)setRightBarButtonItemImage:(UIImage *)rightBarButtonItemImage{
    _rightBarButtonItemImage = rightBarButtonItemImage;
    [self.rightButton setImage:rightBarButtonItemImage forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setRightBarButtonItemTitle:(NSString *)rightBarButtonItemTitle{
    _rightBarButtonItemTitle = rightBarButtonItemTitle;
    CGRect rect= [rightBarButtonItemTitle boundingRectWithSize:CGSizeMake(100, 44) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    CGRect originalRect = self.rightButton.bounds;
    originalRect.size.width = rect.size.width;
    self.rightButton.frame = originalRect;
    [self.rightButton setTitle:rightBarButtonItemTitle forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setLeftBarButtonItemImage:(UIImage *)leftBarButtonItemImage{
    _leftBarButtonItemImage = leftBarButtonItemImage;
    [self.leftButton setImage:leftBarButtonItemImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setLeftBarButtonItemTitle:(NSString *)leftBarButtonItemTitle{
    _leftBarButtonItemTitle = leftBarButtonItemTitle;
    CGRect rect= [leftBarButtonItemTitle boundingRectWithSize:CGSizeMake(100, 44) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
    CGRect originalRect = self.leftButton.bounds;
    originalRect.size.width = rect.size.width;
    self.leftButton.frame = originalRect;

    [self.leftButton setTitle:leftBarButtonItemTitle forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
}

#pragma mark - getter
- (UIButton *)leftButton{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.frame = CGRectMake(0, 0, 44, 44);
        _leftButton.titleLabel.font = [UIFont systemFontOfSize:15];
        _leftButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        _leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_leftButton addTarget:self action:@selector(leftBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftButton;
}

- (UIButton *)rightButton{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.frame = CGRectMake(0, 0, 44, 44);
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
        _rightButton.titleLabel.textAlignment = NSTextAlignmentRight;
        _rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(rightBarButtonItemAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

@end
