//
//  BaseNavigationViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/10.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationViewController : UINavigationController

@end
