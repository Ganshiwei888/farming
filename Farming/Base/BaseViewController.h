//
//  SHBBaseViewController.h
//  SaleTool
//
//  Created by BOX on 16/10/10.
//  Copyright © 2016年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^VariableParametersBlock)(void);

@interface BaseViewController : UIViewController
///导航条左边item image
@property (strong, nonatomic) UIImage * leftBarButtonItemImage;
///导航条左边item title
@property (copy, nonatomic) NSString *leftBarButtonItemTitle;
///导航条右边item image
@property (strong, nonatomic) UIImage *rightBarButtonItemImage;
///导航条右边item title
@property (copy, nonatomic) NSString *rightBarButtonItemTitle;
///不定参数的block
@property (copy, nonatomic) VariableParametersBlock variableParametersBlock;

//没有重新定义左边按钮时执行的方法
- (void)backAction;
///左边按钮执行方法
- (void)leftBarButtonItemAction;

///右边按钮执行方法
- (void)rightBarButtonItemAction;

@end
