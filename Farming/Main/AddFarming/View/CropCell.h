//
//  CropCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Plant;

@protocol CropCellDelegate <NSObject>

- (void)chooseCropWithCell:(UICollectionViewCell *)cell plant:(Plant *)plant;
- (void)cancelCropWithCell:(UICollectionViewCell *)cell plant:(Plant *)plant;

@end

@interface CropCell : UICollectionViewCell

@property (nonatomic, strong) NSArray<Plant *> *dataArray;
@property (nonatomic, weak) id<CropCellDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *selectedArray;

@end
