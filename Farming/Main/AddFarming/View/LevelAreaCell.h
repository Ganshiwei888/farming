//
//  LevelAreaCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Partition;
@class Subarea;

@protocol LevelAreaCellDelegate <NSObject>

@optional

- (void)didSelectedWithPartition:(Partition *)partition indexPath:(NSIndexPath *)indexPath;
- (void)didSelectedWithSubarea:(Subarea *)subArea indexPath:(NSIndexPath *)indexPath;

@end

@interface LevelAreaCell : UITableViewCell

@property (nonatomic, weak) id <LevelAreaCellDelegate> delegate;

- (void)reloadWithPartition:(Partition *)partition indexPath:(NSIndexPath *)indexPath;
- (void)reloadWithSubarea:(Subarea *)subarea indexPath:(NSIndexPath *)indexPath;

@end
