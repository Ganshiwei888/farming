//
//  LevelTwoAreaCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "LevelTwoAreaCell.h"
#import "LevelAreaCell.h"
#import "Subarea.h"

static NSString *const levelTwoAreaCellIdentifier = @"LevelAreaCell";

@interface LevelTwoAreaCell () <UITableViewDataSource, UITableViewDelegate, LevelAreaCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSString *current;

@end

@implementation LevelTwoAreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupTableView];

}

- (void)setupTableView {
    [self.tableView registerNib:[LevelAreaCell defaultNib] forCellReuseIdentifier:levelTwoAreaCellIdentifier];
    self.tableView.tableFooterView = [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LevelAreaCell *cell = [tableView dequeueReusableCellWithIdentifier:levelTwoAreaCellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    Subarea *subarea = self.dataArray[indexPath.row];
    [cell reloadWithSubarea:subarea indexPath:indexPath];
    return cell;
}

#pragma mark -LevelAreaCellDelegate

- (void)didSelectedWithSubarea:(Subarea *)subArea indexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(chooseLevelTwoAreaWithCell: subarea:indexPath:)]) {
        [self.delegate chooseLevelTwoAreaWithCell:self subarea:subArea indexPath:indexPath];
    }
}

#pragma mark -Setter

- (void)setDataArray:(NSArray<Subarea *> *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

@end
