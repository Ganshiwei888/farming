//
//  LevelAreaCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "LevelAreaCell.h"
#import "Partition.h"

@interface LevelAreaCell ()

@property (weak, nonatomic) IBOutlet UILabel *levelArea;
@property (strong, nonatomic) Partition *partition;
@property (strong, nonatomic) Subarea *subarea;
@property (assign, nonatomic) NSInteger type;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) NSIndexPath *indexPath;

@end

@implementation LevelAreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)click:(UIButton *)sender {
    sender.selected = YES;
    if (self.type == 0) {
        if ([self.delegate respondsToSelector:@selector(didSelectedWithPartition:indexPath:)]) {
            [self.delegate didSelectedWithPartition:self.partition indexPath:self.indexPath];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(didSelectedWithSubarea:indexPath:)]) {
            [self.delegate didSelectedWithSubarea:self.subarea indexPath:self.indexPath];
        }
    }
}

- (void)reloadWithPartition:(Partition *)partition indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    if (partition.selected) {
        self.button.selected = YES;
        self.levelArea.textColor = kGreenColor;
    } else {
        self.button.selected = NO;
        self.levelArea.textColor = kFontBlackColor;
    }

    self.levelArea.text = partition.partitionName;
    self.partition = partition;
    self.type = 0;
}

- (void)reloadWithSubarea:(Subarea *)subarea indexPath:(NSIndexPath *)indexPath{
    self.indexPath = indexPath;
    if (subarea.selected) {
        self.button.selected = YES;
        self.levelArea.textColor = kGreenColor;
    } else {
        self.button.selected = NO;
        self.levelArea.textColor = kFontBlackColor;
    }

    self.levelArea.text = subarea.subareaName;
    self.subarea = subarea;
    self.type = 1;
}

@end
