//
//  AgricultrueCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AgricultrueCell.h"
#import "Agriculture.h"

@interface AgricultrueCell ()

@property (weak, nonatomic) IBOutlet UILabel *agricultrual;
@property (weak, nonatomic) IBOutlet UILabel *dosage;
@property (weak, nonatomic) IBOutlet UILabel *unit;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;


@end

@implementation AgricultrueCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (IBAction)delete:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(deleteWithCell:)]) {
        [self.delegate deleteWithCell:self];
    }
}

- (void)reload:(Agriculture *)agriculture enterType:(EnterViewControllerType)enterType{
    self.agricultrual.text = agriculture.agriculturalProductName;
    self.dosage.text = agriculture.agriculturalProductYield;
    self.unit.text = agriculture.unit;
    if (enterType == EnterViewControllerTypeAddFarming) {
        self.deleteButton.hidden = NO;
    } else {
        self.deleteButton.hidden = YES;
    }
}

@end
