//
//  AreaCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AreaCell, Area;

@protocol AreaCellDelegate <NSObject>

- (void)deleteAreaWithCell:(AreaCell *)cell;

@end

@interface AreaCell : UITableViewCell

@property (nonatomic, weak) id<AreaCellDelegate> delegate;

- (void)reload:(Area *)area enterType:(EnterViewControllerType)enterType;

@end
