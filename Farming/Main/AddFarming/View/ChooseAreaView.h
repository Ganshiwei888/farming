//
//  ChooseAreaView.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/18.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Area;
@class Partition;

typedef void(^AreaBlock)(Area *area);

@interface ChooseAreaView : UIView

@property (nonatomic, strong) NSArray<Partition *> *partitionArray;

- (instancetype)initWithView:(UIView *)view areaBlock:(AreaBlock)areaBlock cancelAreaBlock:(AreaBlock)cancelAreaBlock;
- (void)show;
- (void)hide;

@end
