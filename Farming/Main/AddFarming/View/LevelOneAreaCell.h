//
//  LevelOneAreaCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Partition;

@protocol LevelOneAreaCellDelegate <NSObject>

- (void)chooseLevelOneAreaWithCell:(UICollectionViewCell *)cell partition:(Partition *)partition indexPath:(NSIndexPath *)indexPath;

@end

@interface LevelOneAreaCell : UICollectionViewCell

@property (nonatomic, strong) NSArray <Partition *> *dataArray;
@property (nonatomic, weak) id <LevelOneAreaCellDelegate> delegte;
@property (nonatomic, strong) NSArray *selectedArray;

@end
