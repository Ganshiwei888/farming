//
//  LevelTwoAreaCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Subarea;

@protocol LevelTwoAreaCellDelegate <NSObject>

- (void)chooseLevelTwoAreaWithCell:(UICollectionViewCell *)cell subarea:(Subarea *)subarea indexPath:(NSIndexPath *)indexPath;

@end

@interface LevelTwoAreaCell : UICollectionViewCell

@property (nonatomic, strong) NSArray<Subarea *> *dataArray;
@property (nonatomic, weak) id<LevelTwoAreaCellDelegate> delegate;

@end
