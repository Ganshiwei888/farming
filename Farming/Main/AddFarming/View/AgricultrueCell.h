//
//  AgricultrueCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Agriculture, AgricultrueCell;

@protocol AgricultrueCellDelegate <NSObject>

- (void)deleteWithCell:(AgricultrueCell *)cell;

@end

@interface AgricultrueCell : UITableViewCell

@property (nonatomic, weak) id<AgricultrueCellDelegate> delegate;

- (void)reload:(Agriculture *)agriculture enterType:(EnterViewControllerType)enterType;

@end
