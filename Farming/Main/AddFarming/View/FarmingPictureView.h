//
//  FarmingPictureView.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/17.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PictureBlock)(NSArray *pictures);

@interface FarmingPictureView : UIView

@property (nonatomic, strong) PictureBlock pictureBlock;
@property (nonatomic, strong) NSArray *urlImageArray;
@property (nonatomic, assign) EnterViewControllerType enterType;

- (void)reloadWithImages:(NSArray *)images enterType:(EnterViewControllerType)enterType;

@end
