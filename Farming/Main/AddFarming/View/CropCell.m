//
//  CropCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropCell.h"
#import "LevelCropCell.h"
#import "Plant.h"

static NSString *const levelCropCellIdentifier = @"LevelCropCell";

@interface CropCell () <LevelCropCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation CropCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupTableView];

}

- (void)setupTableView {
    [self.tableView registerNib:[LevelCropCell defaultNib] forCellReuseIdentifier:levelCropCellIdentifier];
    self.tableView.tableFooterView = [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LevelCropCell *cell = [tableView dequeueReusableCellWithIdentifier:levelCropCellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    Plant *plant = self.dataArray[indexPath.row];
    DLog(@"%@",plant.productOptionName);
    [cell reloadWithPlant:plant];
    return cell;
}

#pragma mark - LevelCropCellDelegate

- (void)didSelectWithPlant:(Plant *)plant selected:(BOOL)selected {
    if (selected) {
        if ([self.delegate respondsToSelector:@selector(chooseCropWithCell:plant:)]) {
            [self.delegate chooseCropWithCell:self plant:plant];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(cancelCropWithCell:plant:)]) {
            [self.delegate cancelCropWithCell:self plant:plant];
        }
    }
}

#pragma mark - Setter

- (void)setDataArray:(NSArray<Plant *> *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

@end
