//
//  LevelCropCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "LevelCropCell.h"
#import "Plant.h"

@interface LevelCropCell ()

@property (weak, nonatomic) IBOutlet UIImageView *state;
@property (weak, nonatomic) IBOutlet UILabel *crop;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) Plant *plant;

@end

@implementation LevelCropCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)click:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.plant.selected = sender.selected;
    if ([self.delegate respondsToSelector:@selector(didSelectWithPlant:selected:)]) {
        [self.delegate didSelectWithPlant:self.plant selected:sender.selected];
    }
    if (sender.selected) {
        self.state.image = [UIImage imageNamed:@"cropSelected"];
        self.crop.textColor = kGreenColor;
    } else {
        self.state.image = [UIImage imageNamed:@"cropUnselected"];
        self.crop.textColor = kFontBlackColor;
    }
}

- (void)reloadWithPlant:(Plant *)plant {
    self.button.selected = plant.selected;
    if (plant.selected) {
        self.state.image = [UIImage imageNamed:@"cropSelected"];
        self.crop.textColor = kGreenColor;
    } else {
        self.state.image = [UIImage imageNamed:@"cropUnselected"];
        self.crop.textColor = kFontBlackColor;
    }
    self.crop.text = plant.productOptionName;
    self.plant = plant;
}

@end
