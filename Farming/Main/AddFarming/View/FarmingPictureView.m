//
//  FarmingPictureView.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/17.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "FarmingPictureView.h"
#import "UIImageView+addDelete.h"
#import <Masonry.h>
#import <UIImageView+WebCache.h>

@interface FarmingPictureView () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) NSMutableArray *subViewArray;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) NSMutableArray *imageArray;

@end

@implementation FarmingPictureView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self updateView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {

    }
    return self;
}

- (void)updateView {
    [self removeAllSubviews];
    [self.subViewArray removeAllObjects];
    [self addImage];
    [self addButton];
    [self addHelpView];
    [self addConstraint];
}

- (void)editUpdateView {
    [self removeAllSubviews];
    [self.subViewArray removeAllObjects];
    [self setUrlImages];
    [self addHelpView];
    [self addConstraint];
}

- (void)addConstraint {
    CGFloat width = (CGRectGetWidth(self.frame) - 3 * 8) / 4.0;
    CGFloat height = width;
    [self.subViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:width leadSpacing:0 tailSpacing:0];
    [self.subViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(@(height));
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
}

- (void)addImage {
    NSInteger count = self.imageArray.count;
    for (NSInteger i = 0; i < count; i ++) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:self.imageArray[i]];
        if (self.enterType == EnterViewControllerTypeAddFarming) {
            [imageView addDeleteFunction];
        }
        [imageView setDeleteBlock:^(UIImage *deleteImage){
            [self.imageArray removeObject:deleteImage];
            [self updateView];
            [self implemationBlock];
        }];
        [self.subViewArray addObject:imageView];
        [self addSubview:imageView];
    }
}

- (void)setUrlImages {
    NSInteger count = self.urlImageArray.count;
    for (NSInteger i = 0; i < count; i ++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView sd_setImageWithURL:[NSURL URLWithString:self.urlImageArray[i]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error) {
                [self.imageArray addObject:image];
                [self implemationBlock];
            }
        }];
        [self.subViewArray addObject:imageView];
        [self addSubview:imageView];
    }
}

- (void)addButton {
    if (self.subViewArray.count < 4) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:@"AddImage"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(choosePicture) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [self.subViewArray addObject:button];
    }
}

- (void)addHelpView {
    NSInteger count =MAX(0, 4 - self.subViewArray.count);
    for (NSInteger i = 0; i <  count; i ++) {
        UIView *view = [[UIView alloc] init];
        [self addSubview:view];
        [self.subViewArray addObject:view];
    }
}

- (void)reloadWithImages:(NSArray *)images enterType:(EnterViewControllerType)enterType {
    _urlImageArray = images;
    _enterType = enterType;
    [self editUpdateView];
}

- (void)choosePicture {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self startPhotoFromAlbumwithAllowsEditing:YES];
    }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self startTakePhotowithAllowsEditing:YES];
    }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }]];

    [self.viewController presentViewController:alertController animated:YES completion:nil];
}

-(void)startTakePhotowithAllowsEditing:(BOOL)allowsEditing{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        self.imagePickerController.mediaTypes =[NSArray arrayWithObjects:@"public.image", nil];
        self.imagePickerController.delegate = self;
        self.imagePickerController.allowsEditing = allowsEditing;
    }
    [self.viewController presentViewController:self.imagePickerController animated:YES completion:nil];
}

-(void)startPhotoFromAlbumwithAllowsEditing:(BOOL)allowsEditing{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSArray *tempTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePickerController.sourceType];
        self.imagePickerController.mediaTypes = tempTypes;
        self.imagePickerController.delegate = self;
        self.imagePickerController.allowsEditing = allowsEditing;
    }
    [self.viewController presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)implemationBlock {
    if (self.pictureBlock) {
        self.pictureBlock(self.imageArray);
    }
}
#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *mediaType = info[UIImagePickerControllerMediaType];

    [picker dismissViewControllerAnimated:YES completion:nil];

    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = info[@"UIImagePickerControllerEditedImage"];
        UIImage *OriginalImage = info[@"UIImagePickerControllerOriginalImage"];
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            [self saveImageToPhotos:OriginalImage];
        }
        [self.imageArray addObject:image];
        [self updateView];
        [self implemationBlock];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveImageToPhotos:(UIImage*)savedImage {

    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextIn {
    if(error != NULL){
        DLog(@"保存图片失败");
    }else{
        DLog(@"保存图片成功");
    }
}

#pragma mark -Setter

- (void)setEnterType:(EnterViewControllerType)enterType {
    _enterType = enterType;
    [self updateView];

}

#pragma mark - Getter
- (NSMutableArray *)imageArray {
    if (!_imageArray) {
        _imageArray = [[NSMutableArray alloc] init];
    }
    return _imageArray;
}

- (NSMutableArray *)subViewArray {
    if (!_subViewArray) {
        _subViewArray = [[NSMutableArray alloc] init];
    }
    return _subViewArray;
}

- (UIImagePickerController *)imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
    }
    return  _imagePickerController;
}

@end
