//
//  LevelOneAreaCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "LevelOneAreaCell.h"
#import "LevelAreaCell.h"
#import "Partition.h"

static NSString *const levelAreaCellIdentifier = @"LevelAreaCell";

@interface LevelOneAreaCell () <UITableViewDelegate, UITableViewDataSource, LevelAreaCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSString *current;

@end

@implementation LevelOneAreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupTableView];
    
}

- (void)setupTableView {
    [self.tableView registerNib:[LevelAreaCell defaultNib] forCellReuseIdentifier:levelAreaCellIdentifier];
    self.tableView.tableFooterView = [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LevelAreaCell *cell = [tableView dequeueReusableCellWithIdentifier:levelAreaCellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    Partition *partition = self.dataArray[indexPath.row];
    [cell reloadWithPartition:partition indexPath:indexPath];
    return cell;
}

#pragma mark - LevelAreaCellDelegate

- (void)didSelectedWithPartition:(Partition *)partition indexPath:(NSIndexPath *)indexPath{
    if ([self.delegte respondsToSelector:@selector(chooseLevelOneAreaWithCell:partition:indexPath:)]) {
        [self.delegte chooseLevelOneAreaWithCell:self partition:partition indexPath:indexPath];
    }
}

#pragma mark - Setter

- (void)setDataArray:(NSArray<Partition *> *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}


@end
