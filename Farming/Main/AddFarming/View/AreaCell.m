//
//  AreaCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AreaCell.h"
#import "Area.h"
#import "Partition.h"

@interface AreaCell ()

@property (weak, nonatomic) IBOutlet UILabel *levelOne;
@property (weak, nonatomic) IBOutlet UILabel *leveltwo;
@property (weak, nonatomic) IBOutlet UILabel *crop;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;


@end

@implementation AreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)delete:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(deleteAreaWithCell:)]) {
        [self.delegate deleteAreaWithCell:self];
    }
}

- (void)reload:(Area *)area enterType:(EnterViewControllerType)enterType{
    self.levelOne.text = area.partition.partitionName;
    self.leveltwo.text = area.subarea.subareaName;
    self.crop.text = area.plant.productOptionName;
    if (enterType == EnterViewControllerTypeAddFarming) {
        self.deleteButton.hidden = NO;
    } else {
        self.deleteButton.hidden = YES;
    }
}

@end
