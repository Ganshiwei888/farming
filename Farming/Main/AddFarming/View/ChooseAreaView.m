//
//  ChooseAreaView.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/18.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "ChooseAreaView.h"
#import "LevelOneAreaCell.h"
#import "LevelTwoAreaCell.h"
#import "CropCell.h"
#import "Area.h"
#import "Partition.h"

static NSString *const levelOneAreaCellIdentifier = @"LevelOneAreaCell";
static NSString *const levelTwoAreaCellIdentifier = @"LevelTwoAreaCell";
static NSString *const cropCellIdentifier = @"CropCell";

@interface ChooseAreaView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LevelOneAreaCellDelegate, LevelTwoAreaCellDelegate, CropCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *levelOneArea;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIButton *levelTwoArea;
@property (weak, nonatomic) IBOutlet UIButton *crop;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *levelOneAreaWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *levelTwoAreaWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewWidthConstraint;

@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) NSArray<UIButton *> *buttonArray;
@property (strong, nonatomic) AreaBlock areaBlock;
@property (strong, nonatomic) AreaBlock cancelAreaBlock;
@property (strong, nonatomic) Plant *plant;
@property (strong, nonatomic) Subarea *subarea;
@property (strong, nonatomic) Partition *partition;
@property (strong, nonatomic) Area *currentArea;
@property (assign, nonatomic) NSInteger subareaIndex;
@property (assign, nonatomic) NSInteger plantIndex;
@property (assign, nonatomic) NSInteger rowCount;

@end

@implementation ChooseAreaView


- (instancetype)initWithView:(UIView *)view areaBlock:(AreaBlock)areaBlock cancelAreaBlock:(AreaBlock)cancelAreaBlock{
    if (self = [super init]) {
        self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
        _contentView = view;
        _areaBlock = areaBlock;
        _cancelAreaBlock = cancelAreaBlock;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.rowCount = 1;
    [self setupCollectionView];
    [self setupData];
    [kNotificationCenter addObserver:self selector:@selector(deleteArea:) name:kDeleteArea object:nil];
}

- (void)setupData {
    self.buttonArray = @[self.levelOneArea, self.levelTwoArea, self.crop];
}

- (void)setupCollectionView {
    [self.collectionView registerNib:[LevelOneAreaCell defaultNib] forCellWithReuseIdentifier:levelOneAreaCellIdentifier];
    [self.collectionView registerNib:[LevelTwoAreaCell defaultNib] forCellWithReuseIdentifier:levelTwoAreaCellIdentifier];
    [self.collectionView registerNib:[CropCell defaultNib] forCellWithReuseIdentifier:cropCellIdentifier];
}

- (void)show {
    __block BOOL needAdd = YES;
    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[ChooseAreaView class]]) {
            needAdd = NO;
            *stop = YES;
        }
    }];

    CGFloat height = KScreenHeight / 2.0;
    if (needAdd) {
        self.frame = CGRectMake(0, KScreenHeight, KScreenWidth, height);
        [self.contentView addSubview:self];
        [UIView animateWithDuration:0.5 animations:^{
            self.frame = CGRectMake(0, height, KScreenWidth, height);
        }];
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            self.frame = CGRectMake(0, height, KScreenWidth, height);
        }];
    }
    [self layoutIfNeeded];
    [self.collectionView reloadData];
}

- (void)hide {
    [self Done:nil];
}

- (void)deleteArea:(NSNotification *)notification {
    Area *area = (Area *)notification.object;
    [self.partitionArray enumerateObjectsUsingBlock:^(Partition * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj.partitionId isEqualToString: area.partition.partitionId]) {
            [obj.subarea enumerateObjectsUsingBlock:^(Subarea * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                if ([obj.subareaId isEqualToString:area.subarea.subareaId]) {
                    [obj.plants enumerateObjectsUsingBlock:^(Plant * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([obj.plantArchivesId isEqualToString: area.plant.plantArchivesId]) {
                            obj.selected = NO;
                            *stop = YES;
                        }
                    }];
//                    *stop = YES;
//                }
            }];
//            *stop = YES;
//        }
    }];
    if (self.rowCount == 3) {
    [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:2 inSection:0]]];
    }
}
#pragma mark - ButtonAction
//完成
- (IBAction)Done:(id)sender {
    CGFloat height = KScreenHeight / 2.0;
    [UIView animateWithDuration:0.5 animations:^{
        self.frame = CGRectMake(0, KScreenHeight, KScreenWidth, height);
    }];
}

- (IBAction)levelOneAreaAction:(UIButton *)sender {
    [self lineAnimation:0];
    [self scrollToLeftWithItem:0];
}

- (IBAction)levelTwoAreaAction:(UIButton *)sender {
    [self lineAnimation:1];
    [self scrollToLeftWithItem:1];
}

- (IBAction)cropAction:(UIButton *)sender {
    [self lineAnimation:2];
    [self scrollToLeftWithItem:2];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.rowCount;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.item) {
        case 0:{
            LevelOneAreaCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:levelOneAreaCellIdentifier forIndexPath:indexPath];
            cell.dataArray = self.partitionArray;
            cell.delegte = self;
            return cell;
        }
            break;
        case 1:{
            LevelTwoAreaCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:levelTwoAreaCellIdentifier forIndexPath:indexPath];
            cell.dataArray = self.partitionArray[self.subareaIndex].subarea;
            cell.delegate = self;
            return cell;
        }
            break;
        case 2:{
            CropCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cropCellIdentifier forIndexPath:indexPath];
            cell.dataArray = self.partitionArray[self.subareaIndex].subarea[self.plantIndex].plants;
            cell.delegate = self;
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(KScreenWidth, CGRectGetHeight(self.collectionView.frame));

}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        NSInteger item = ceil(scrollView.contentOffset.x / KScreenWidth);
        [self lineAnimation:item];
    }
}

#pragma mark - CellDelegate

- (void)chooseLevelOneAreaWithCell:(UICollectionViewCell *)cell partition:(Partition *)partition indexPath:(NSIndexPath *)indexPath{
    [self handleLevelOneStateWithPartition:partition];
    self.subareaIndex = indexPath.row;
    if (self.currentArea.partition.partitionId != partition.partitionId) {
        self.rowCount = 2;
        self.crop.hidden = YES;
        [self.levelTwoArea setTitle:@"请选择" forState:UIControlStateNormal];
        CGFloat width = [@"请选择" widthForFont:[UIFont systemFontOfSize:18]];
        self.levelTwoAreaWidthContraint.constant = width;
        [self.collectionView reloadData];
    }
    self.currentArea.partition = partition;
    self.levelTwoArea.hidden = NO;
    CGFloat width = MAX(34, [partition.partitionName widthForFont:[UIFont systemFontOfSize:18]]);
    self.levelOneAreaWidthConstraint.constant = width;
    [self.topView layoutIfNeeded];
    [self.levelOneArea setTitle:partition.partitionName forState:UIControlStateNormal];
    [self scrollToLeftWithItem:1];
    [self lineAnimation:1];
}

- (void)chooseLevelTwoAreaWithCell:(UICollectionViewCell *)cell subarea:(Subarea *)subarea indexPath:(NSIndexPath *)indexPath{
    [self handleLevelTwoStateWithSubarea:subarea];
    self.plantIndex = indexPath.row;
    if (self.rowCount == 2) {
        self.rowCount = 3;
    }
    [self.collectionView reloadData];
    self.currentArea.subarea = subarea;
    self.crop.hidden = NO;
    CGFloat width = MAX(34,[subarea.subareaName widthForFont:[UIFont systemFontOfSize:18]]);
    self.levelTwoAreaWidthContraint.constant = width;
    [self.topView layoutIfNeeded];
    [self.levelTwoArea setTitle:subarea.subareaName forState:UIControlStateNormal];
    [self scrollToLeftWithItem:2];
    [self lineAnimation:2];
}

- (void)chooseCropWithCell:(UICollectionViewCell *)cell plant:(Plant *)plant{
    Area *area = [[Area alloc] init];
    area.partition = self.currentArea.partition;
    area.subarea = self.currentArea.subarea;
    area.plant = plant;

    if (self.areaBlock) {
        self.areaBlock(area);
    }
}

- (void)cancelCropWithCell:(UICollectionViewCell *)cell plant:(Plant *)plant{
    Area *area = [[Area alloc] init];
    area.partition = self.currentArea.partition;
    area.subarea = self.currentArea.subarea;
    area.plant = plant;

    if (self.cancelAreaBlock) {
        self.cancelAreaBlock(area);
    }
}

#pragma mark - PrivatedMethod

- (void)handleButtonStated:(UIButton *)button {
    [self.buttonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = NO;
        if (obj == button) {
            obj.selected = YES;
        }
    }];
}

- (void)lineAnimation:(NSInteger )destinationItem {
    self.lineViewWidthConstraint.constant = CGRectGetWidth(self.buttonArray[destinationItem].frame);
    self.lineViewCenterXConstraint.constant = CGRectGetCenter(self.buttonArray[destinationItem].frame).x - CGRectGetCenter(self.levelOneArea.frame).x;
    [UIView animateWithDuration:0.3 animations:^{
        [self.topView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self handleButtonStated:self.buttonArray[destinationItem]];
    }];
}

- (void)scrollToLeftWithItem:(NSInteger)item {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

- (void)handleLevelOneStateWithPartition:(Partition *)partition {
    [self.partitionArray enumerateObjectsUsingBlock:^(Partition * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = NO;
        if (obj.partitionId == partition.partitionId) {
            obj.selected = YES;
        }
        [obj.subarea enumerateObjectsUsingBlock:^(Subarea * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.selected = NO;
        }];
    }];
}

- (void)handleLevelTwoStateWithSubarea:(Subarea *)subarea {
    [self.partitionArray enumerateObjectsUsingBlock:^(Partition * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj.subarea enumerateObjectsUsingBlock:^(Subarea * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.selected = NO;
            if (obj.subareaId == subarea.subareaId) {
                obj.selected = YES;
            }
        }];
    }];
}

#pragma mark - Setter

- (void)setPartitionArray:(NSArray<Partition *> *)partitionArray {
    _partitionArray = partitionArray;
    [self.collectionView reloadData];
}

#pragma mark - Getter

- (Area *)currentArea {
    if (!_currentArea) {
        _currentArea = [[Area alloc] init];
    }
    return _currentArea;
}

@end
