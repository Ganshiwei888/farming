//
//  LevelCropCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Plant;

@protocol LevelCropCellDelegate <NSObject>

- (void)didSelectWithPlant:(Plant *)plant selected:(BOOL)selected;

@end

@interface LevelCropCell : UITableViewCell

@property (nonatomic, weak) id<LevelCropCellDelegate> delegate;

- (void)reloadWithPlant:(Plant *)plant;

@end
