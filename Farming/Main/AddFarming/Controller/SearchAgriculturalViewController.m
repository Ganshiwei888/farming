//
//  SearchAgriculturalViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/6.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "SearchAgriculturalViewController.h"
#import "Agriculture.h"

@interface SearchAgriculturalViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSArray<Agriculture *> *dataArray;

@end

@implementation SearchAgriculturalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    self.tableView.tableFooterView = [UIView new];
}

- (void)setupNavigation {
    self.searchBar =[[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 2 * 44 - 2 * 15, 44)];
    self.searchBar.placeholder = @"请输入搜索内容";
    self.searchBar.tintColor = kGreenColor;
    self.searchBar.delegate = self;
    if ([self.searchBar canBecomeFirstResponder]) {
        [self.searchBar becomeFirstResponder];
    }
    UIView *wrapView = [[UIView alloc] initWithFrame:self.searchBar.frame];
    [wrapView addSubview:self.searchBar];
    self.navigationItem.titleView = wrapView;
    self.rightBarButtonItemTitle = @"取消";

}

- (void)rightBarButtonItemAction {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self setupData:searchBar.text];
}

- (void)setupData:(NSString *)searchName {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"agriculturalProductCategory"] = self.agricultureCategory;
    parameters[@"agriculturalProductName"] = searchName;
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kAgricultrue parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.dataArray = [Agriculture mj_objectArrayWithKeyValuesArray:baseResponse.data];
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    Agriculture *agriculture = self.dataArray[indexPath.row];
    cell.textLabel.text = agriculture.agriculturalProductName;
    cell.textLabel.textColor = kFontBlackColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Agriculture *agriculture = self.dataArray[indexPath.row];
    [kNotificationCenter postNotificationName:kSearchAgriculture object:agriculture];
    NSArray<UIViewController *> *viewControllers = self.navigationController.viewControllers;
    NSInteger count = viewControllers.count - 1;
    [self.navigationController popToViewController:viewControllers[count - 2] animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}


@end
