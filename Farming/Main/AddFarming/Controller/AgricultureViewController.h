//
//  AgricultureViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseViewController.h"

@class Agriculture;

typedef void(^ChooseAgricultureBlock)(Agriculture *agriculture);

@interface AgricultureViewController : BaseViewController

@property (nonatomic, copy) NSString *agricultureCategory;
@property (nonatomic, strong) ChooseAgricultureBlock chooseAgricultureBlock;

@end
