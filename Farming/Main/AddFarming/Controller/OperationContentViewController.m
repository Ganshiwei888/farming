//
//  OperationContentViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "OperationContentViewController.h"
#import "CommonOperationContent.h"
#import "JCTagListView.h"
#import "SearchViewController.h"

@interface OperationContentViewController ()

@property (weak, nonatomic) IBOutlet JCTagListView *commonListView;
@property (weak, nonatomic) IBOutlet JCTagListView *chooseListView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commonLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commonListViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseListViewHeightConstraint;
@property (strong, nonatomic) JCCollectionViewTagFlowLayout *flowLayout;
@property (strong, nonatomic) NSMutableArray *commonContents;
@property (strong, nonatomic) NSMutableArray *chooseContents;
@property (strong, nonatomic) NSMutableArray<CommonOperationContent *> * dataArray;


@end

@implementation OperationContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    [self setupData];
}

- (void)setupNavigation {
    self.navigationItem.title = @"操作内容";
    self.rightBarButtonItemImage = [UIImage imageNamed:@"search"];
}

- (void)setupData {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kOperationContent parameters:nil success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.dataArray = [CommonOperationContent mj_objectArrayWithKeyValuesArray:baseResponse.data];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupViews];
            });
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (void)setupViews {
    self.flowLayout = [[JCCollectionViewTagFlowLayout alloc] init];

    RLMResults <CommonOperationContent *> *commonContents = [CommonOperationContent allObjects];
    NSInteger count = commonContents.count;
    if (count != 0) {
       commonContents = [commonContents sortedResultsUsingKeyPath:@"date" ascending:NO];
        for (NSInteger i = 0; i < count; i ++) {
            CommonOperationContent *content = commonContents[i];
            [self.commonContents addObject:content.farmworkTypeName];
        }
    }

    NSInteger chooseCount = self.dataArray.count;
    for (NSInteger i = 0; i < chooseCount; i ++) {
        CommonOperationContent *content = self.dataArray[i];
        [self.chooseContents addObject: content.farmworkTypeName];
    }
    float commonHeight = [self.flowLayout calculateContentHeight:self.commonContents];
    float chooseHeight = [self.flowLayout calculateContentHeight:self.chooseContents];

    if (!kStringIsEmpty(self.contentStr)) {
        if (self.type == OperationContentTypeChoose) {
            [self.chooseContents enumerateObjectsUsingBlock:^(NSString  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isEqualToString:self.contentStr]) {
                    [self.chooseListView setSelectedTag:self.contentStr];
                    *stop = YES;
                }
            }];
        } else {
            [self.commonContents enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isEqualToString:self.contentStr]) {
                    [self.commonListView setSelectedTag:self.contentStr];
                    *stop = YES;
                }
            }];
        }
    }

    [self.chooseListView setTags:self.chooseContents];
    [self.commonListView setTags:self.commonContents];
    if (count == 0) {
        self.commonLabelHeightConstraint.constant = 0;
        self.commonListViewHeightConstraint.constant = 0;
    } else {
        self.commonLabelHeightConstraint.constant = 37;
        self.commonListViewHeightConstraint.constant = commonHeight;
    }

    self.chooseListViewHeightConstraint.constant = chooseHeight;

    [self.view layoutIfNeeded];
    [self.chooseListView.collectionView reloadData];
    [self.commonListView.collectionView reloadData];

    [self.commonListView setCompletionBlockWithSelected:^(NSInteger index) {
        if ([self.delegate respondsToSelector:@selector(chooseOperationContentWithContent:type:)]) {
            [self.delegate chooseOperationContentWithContent:commonContents[index] type:OperationContentTypeCommon];
            [self backAction];
        }
    }];

    [self.chooseListView setCompletionBlockWithSelected:^(NSInteger index) {
        CommonOperationContent *operationContent = self.dataArray[index];
        operationContent.date = [NSDate date];
        [Util handleCommonOperation:operationContent];
        if ([self.delegate respondsToSelector:@selector(chooseOperationContentWithContent:type:)]) {
            [self.delegate chooseOperationContentWithContent:operationContent type:OperationContentTypeChoose];
            [self backAction];
        }
    }];
}

- (void)rightBarButtonItemAction {
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    [self.navigationController pushViewController:searchViewController animated:YES];
}

#pragma mark - Getter

- (NSMutableArray *)commonContents {
    if (!_commonContents) {
        _commonContents = [[NSMutableArray alloc] init];
    }
    return _commonContents;
}

- (NSMutableArray *)chooseContents {
    if (!_chooseContents) {
        _chooseContents =  [[NSMutableArray alloc] init];
    }
    return _chooseContents;
}

@end
