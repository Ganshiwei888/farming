//
//  AddAgriculturalViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/17.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AddAgriculturalViewController.h"
#import "JCTagListView.h"
#import "Agriculture.h"
#import "AgricultureCategory.h"
#import "AgricultureViewController.h"

@interface AddAgriculturalViewController ()
@property (weak, nonatomic) IBOutlet UILabel *agricultureCategory;
@property (weak, nonatomic) IBOutlet JCTagListView *agricultureListView;
@property (weak, nonatomic) IBOutlet UILabel *agriculture;
@property (weak, nonatomic) IBOutlet UITextField *agricultureDosage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agricultureListViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *dosageUnit;
@property (strong, nonatomic) NSMutableArray *agricultureArray;
@property (strong, nonatomic) NSMutableArray<AgricultureCategory *> *agricultureCategoryArray;
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) Agriculture *agricultureModel;

@end

@implementation AddAgriculturalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加农资";
    [self setupAgricultureListView];
    [self addObserver];
    self.index = -1;
}

- (void)dealloc {
    [kNotificationCenter removeObserver:self];
}

- (void)setupAgricultureListView {
    self.agricultureListView.tags = self.agricultureArray;
    JCCollectionViewTagFlowLayout *layout = [[JCCollectionViewTagFlowLayout alloc] init];
    CGFloat height = [layout calculateContentHeight:self.agricultureArray];
    self.agricultureListViewHeightConstraint.constant = height;
    [self.agricultureListView layoutIfNeeded];
    [self.agricultureListView setCompletionBlockWithSelected:^(NSInteger index) {
        self.agricultureCategory.text = self.agricultureArray[index];
        self.index = index;
    }];
}

- (void)addObserver {
    [kNotificationCenter addObserver:self selector:@selector(searchAgriculture:) name:kSearchAgriculture object:nil];
}

- (IBAction)chooseAgriculture:(UIButton *)sender {
    AgricultureViewController *agricultureViewController = [[AgricultureViewController alloc] init];
    [agricultureViewController setChooseAgricultureBlock:^(Agriculture *agriculture){
        self.agricultureModel = agriculture;
        self.agriculture.text = agriculture.agriculturalProductName;
        self.dosageUnit.text = agriculture.unit;
    }];
    if (self.index == -1) {
        agricultureViewController.agricultureCategory = @"";
    } else {
        agricultureViewController.agricultureCategory = self.agricultureCategoryArray[self.index].englishName;
    }
    [self.navigationController pushViewController:agricultureViewController animated:YES];
}

- (IBAction)commit:(UIButton *)sender {
    if (![self whetherSubmissionConditionsAreMet]) {
        return;
    }
    self.agricultureModel.agriculturalProductName = self.agriculture.text;
    self.agricultureModel.agriculturalProductYield = self.agricultureDosage.text;
    self.agricultureModel.unit = self.dosageUnit.text;
    if (self.agricultureBlock) {
        self.agricultureBlock(self.agricultureModel);
        [self backAction];
    }
    
}

- (BOOL)whetherSubmissionConditionsAreMet {
    if ([self.agriculture.text containsString:@"请选择"]) {
        [MBProgressHUD showMessage:@"请选择农资"];
        return NO;
    }

    if (kStringIsEmpty(self.agricultureDosage.text)) {
        [MBProgressHUD showMessage:@"请输入农资用量"];
        return NO;
    }
    return YES;
}

- (void)searchAgriculture:(NSNotification *)notification {
    Agriculture *agriculture = (Agriculture *)notification.object;
    self.agricultureModel = agriculture;
    self.agriculture.text = agriculture.agriculturalProductName;
    self.dosageUnit.text = agriculture.unit;
}

#pragma mark - Getter

- (NSMutableArray *)agricultureArray {
    if (!_agricultureArray) {
        _agricultureArray = [@[@"农药", @"肥料", @"种子", @"其他"] mutableCopy];
    }
    return _agricultureArray;
}

- (NSMutableArray *)agricultureCategoryArray {
    if (!_agricultureCategoryArray) {
        AgricultureCategory *category1 = [self agricultureCategoryWithChineseName:@"农药" englishName:@"pesticide"];
        AgricultureCategory *category2 = [self agricultureCategoryWithChineseName:@"肥料" englishName:@"fertilizer"];
        AgricultureCategory *category3 = [self agricultureCategoryWithChineseName:@"种子" englishName:@"seed"];
        AgricultureCategory *category4 = [self agricultureCategoryWithChineseName:@"其他" englishName:@"other"];
        _agricultureCategoryArray = [@[category1, category2, category3, category4] mutableCopy];
    }
    return _agricultureCategoryArray;
}

- (AgricultureCategory *)agricultureCategoryWithChineseName:(NSString *)chineseName englishName:(NSString *)englishName {
    AgricultureCategory *agricultureCategory = [[AgricultureCategory alloc] init];
    agricultureCategory.chineseName = chineseName;
    agricultureCategory.englishName = englishName;
    return agricultureCategory;
}

@end
