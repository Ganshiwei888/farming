//
//  AddFarmingViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/14.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AddFarmingViewController.h"
#import "OperationContentViewController.h"
#import "AddAgriculturalViewController.h"
#import "ChooseAreaView.h"
#import "Agriculture.h"
#import "AgricultrueLogical.h"
#import "FarmingPictureView.h"
#import "CommonOperationContent.h"
#import "Partition.h"
#import "Area.h"
#import "FarmingDetail.h"

static CGFloat const kBottomConstraint = 50;

@interface AddFarmingViewController () <OperationContentViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commitButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *areaTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agriculturalTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *operationContent;
@property (weak, nonatomic) IBOutlet UILabel *operationTime;
@property (weak, nonatomic) IBOutlet UITextView *markTextView;
@property (weak, nonatomic) IBOutlet UITableView *areaTableView;
@property (weak, nonatomic) IBOutlet UITableView *agriculturalTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet FarmingPictureView *pictureView;
@property (weak, nonatomic) IBOutlet UIButton *commitButton;

@property (weak, nonatomic) IBOutlet UIButton *operationContentButton;
@property (weak, nonatomic) IBOutlet UIButton *operationAreaButton;
@property (weak, nonatomic) IBOutlet UIButton *operationTimeButton;
@property (weak, nonatomic) IBOutlet UIButton *agricultrueButton;
@property (weak, nonatomic) IBOutlet UIImageView *operationContentArrow;
@property (weak, nonatomic) IBOutlet UILabel *operatitonAreaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *operationTimeArrow;
@property (weak, nonatomic) IBOutlet UILabel *agricultureLabel;


@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) ChooseAreaView *chooseAreaView;
@property (strong, nonatomic) NSMutableArray<Area *> *areaArray;
@property (strong, nonatomic) NSArray *pictureArray;
@property (strong, nonatomic) NSMutableArray<Agriculture *> *agricultureArray;
@property (strong, nonatomic) AgricultrueLogical *agricultureLogical;
@property (strong, nonatomic) AgricultrueLogical *areaLogical;
@property (strong, nonatomic) CommonOperationContent *commonOperationContent;
@property (strong, nonatomic) NSArray<Partition *> *partitionArray;
@property (assign, nonatomic) OperationContentType type;
@property (strong, nonatomic) FarmingDetail *farmingDetail;
@property (assign, nonatomic) NSInteger markCount;


@end

@implementation AddFarmingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    [self setupSubViews];
    [self setupDatePicker];
    [self setupTableView];
    [self setupPictureView];
    [self fillViews];
    [self setupData];
    [self addObserver];
}

- (void)dealloc {
    [kNotificationCenter removeObserver:self];
}

- (void)setupNavigation {
    self.leftBarButtonItemImage = [UIImage imageNamed:@"return"];
    if (self.enterType == EnterViewControllerTypeFarmingDetail) {
        self.navigationItem.title = @"农事详情";
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake(0, 0, 44, 44);
        rightButton.titleLabel.font = [UIFont systemFontOfSize:15];
        rightButton.titleLabel.textAlignment = NSTextAlignmentRight;
        rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [rightButton setTitle:@"编辑" forState:UIControlStateNormal];
        [rightButton setTitle:@"保存" forState:UIControlStateSelected];
        [rightButton addTarget:self action:@selector(rightBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    } else if (self.enterType == EnterViewControllerTypeCropDetail) {
        self.navigationItem.title = @"农事详情";
    }
}

- (void)setupSubViews {
    self.deleteButtonHeightConstraint.constant = 0;
    if (self.enterType == EnterViewControllerTypeAddFarming) {
        self.scrollBottomConstraint.constant = kBottomConstraint;
        self.commitButtonHeightConstraint.constant = kBottomConstraint;
    } else {
        self.scrollBottomConstraint.constant = 0;
        self.commitButtonHeightConstraint.constant = 0;
        self.operationContentButton.userInteractionEnabled = NO;
        self.operationAreaButton.userInteractionEnabled = NO;
        self.operationTimeButton.userInteractionEnabled = NO;
        self.agricultrueButton.userInteractionEnabled = NO;
        self.markTextView.userInteractionEnabled = NO;
        self.operationContentArrow.hidden = YES;
        self.operatitonAreaLabel.hidden = YES;
        self.operationTimeArrow.hidden = YES;
        self.agricultureLabel.hidden = YES;
        self.commitButton.hidden = YES;
    }
    [self.view layoutIfNeeded];
}

- (void)setupDatePicker {
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.datePicker setCalendar:[NSCalendar currentCalendar]];
}

- (void)setupTableView {
    self.agricultureLogical = [[AgricultrueLogical alloc] initWithTableView:self.agriculturalTableView tableViewType:AgricultureTableViewType deleteBlock:^(NSInteger row) {
        [self.agricultureArray removeObjectAtIndex:row];
        [self reloadAgricultrualTableView];
    }];
    self.agricultureLogical.enterType = self.enterType;
    self.agriculturalTableView.delegate = self.agricultureLogical;
    self.agriculturalTableView.dataSource = self.agricultureLogical;

    self.areaLogical = [[AgricultrueLogical alloc] initWithTableView:self.areaTableView tableViewType:AreaTableViewType deleteBlock:^(NSInteger row) {
        [kNotificationCenter postNotificationName:kDeleteArea object:self.areaArray[row]];
        [self.areaArray removeObjectAtIndex:row];
        [self reloadAreaTableView];
    }];
    self.areaLogical.enterType = self.enterType;
    self.areaTableView.delegate = self.areaLogical;
    self.areaTableView.dataSource = self.areaLogical;
}

- (void)setupPictureView {
    [self.pictureView setPictureBlock:^(NSArray *pictures) {
        self.pictureArray = [NSArray arrayWithArray:pictures];
    }];
}

- (void)fillViews {
    if (self.enterType == EnterViewControllerTypeFarmingDetail || self.enterType == EnterViewControllerTypeCropDetail) {
        [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        parameters[@"farmworkId"] = self.farmworkId;
        [[RKNetworkHelper shareManager] POST:kFarmingDetail parameters:parameters success:^(BaseResponse *baseResponse) {
            [MBProgressHUD hideHUDForView:self.view];
            if (baseResponse.code == 0) {
                self.farmingDetail = [FarmingDetail initWithDic:baseResponse.data];
                self.operationContent.text = self.farmingDetail.typeName;
                self.date = [NSDate dateWithTimeIntervalSince1970:self.farmingDetail.operateTime];
                self.commonOperationContent = self.farmingDetail.operationContent;
                self.areaLogical.dataArray = self.farmingDetail.areas;
                self.areaArray = self.farmingDetail.areas;
                [self reloadAreaTableView];
                self.operationTime.text = self.farmingDetail.operationTimeStr;
                [self.pictureView reloadWithImages:self.farmingDetail.image enterType:self.enterType];
                self.agricultureLogical.dataArray = self.farmingDetail.agriculturalProduct;
                self.agricultureArray = [self.farmingDetail.agriculturalProduct mutableCopy];
                [self reloadAgricultrualTableView];
                self.markTextView.text = self.farmingDetail.remarks;
            }
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view];
        }];
    }
}

- (void)setupData {
//    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    self.markCount = 0;
    [[RKNetworkHelper shareManager] POST:kArea parameters:nil success:^(BaseResponse *baseResponse) {
//        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.partitionArray = [Partition mj_objectArrayWithKeyValuesArray:baseResponse.data];
            self.chooseAreaView.partitionArray = self.partitionArray;
        }
    } failure:^(NSError *error) {
//        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (void)addObserver {
    [kNotificationCenter addObserver:self selector:@selector(searchOperationContent:) name:kSearchOperationContent object:nil];
}
#pragma mark - ButtonAction
//返回按钮
- (void)leftBarButtonItemAction {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (void)rightBarButtonItemAction:(UIButton *)sender {
    if (!sender.selected) {
        [MBProgressHUD showMessage:@"现可对农事进行编辑"];
         self.navigationItem.title = @"编辑农事";
        self.deleteButtonHeightConstraint.constant = 40;
//        self.operationContentButton.userInteractionEnabled = YES;;
        self.operationAreaButton.userInteractionEnabled = YES;
        self.operationTimeButton.userInteractionEnabled = YES;
        self.agricultrueButton.userInteractionEnabled = YES;
        self.markTextView.userInteractionEnabled = YES;
//        self.operationContentArrow.hidden = NO;
        self.operatitonAreaLabel.hidden = NO;
        self.operationTimeArrow.hidden = NO;
        self.agricultureLabel.hidden = NO;
        self.agricultureLogical.enterType = EnterViewControllerTypeAddFarming;
        self.areaLogical.enterType = EnterViewControllerTypeAddFarming;
        self.pictureView.enterType = EnterViewControllerTypeAddFarming;
    } else {
        [self editFarming];
    }
    [self.view layoutIfNeeded];
    sender.selected = YES;
}

//操作内容
- (IBAction)operationContentAction:(UIButton *)sender {
    OperationContentViewController *operationContentViewController = [[OperationContentViewController alloc] init];
    operationContentViewController.delegate = self;
    operationContentViewController.contentStr = self.commonOperationContent.farmworkTypeName;
    operationContentViewController.type = self.type;
    [self.navigationController pushViewController:operationContentViewController animated:YES];
}

//选择区域
- (IBAction)chooseAreaAction:(UIButton *)sender {
    [self frendlyData];
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self.chooseAreaView show];
    } else {
        [self.chooseAreaView hide];
    }
}

//datePicker
- (IBAction)dateChange:(UIDatePicker *)sender {
    NSDate *date = sender.date;
    self.date = date;
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    self.operationTime.text = dateStr;
}

//操作时间
- (IBAction)operationTimeAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.datePicker.hidden = NO;
        self.datePickerHeightConstraint.constant = 150;
        self.datePickerTopConstraint.constant = 1;
    } else {
        self.datePicker.hidden = YES;
        self.datePickerHeightConstraint.constant = 0;
        self.datePickerTopConstraint.constant = 0;
    }

    [UIView animateWithDuration:0.3 animations:^{
        [self.scrollView layoutIfNeeded];
    }];
}

//添加农资
- (IBAction)addAgriculturalAction:(UIButton *)sender {
    AddAgriculturalViewController *addAgriculturalViewController = [[AddAgriculturalViewController alloc] init];
    [addAgriculturalViewController setAgricultureBlock:^(Agriculture *agriculture){
        [self.agricultureArray addObject:agriculture];
        self.agricultureLogical.dataArray = self.agricultureArray;
        [self reloadAgricultrualTableView];
    }];
    [self.navigationController pushViewController:addAgriculturalViewController animated:YES];
}

- (IBAction)deleteFarming:(id)sender {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"farmworkId"] = self.farmworkId;
    [[RKNetworkHelper shareManager] POST:kDeleteFarming parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            [self backAction];
        } else {
            [MBProgressHUD showMessage:baseResponse.msg];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (IBAction)commitFarming:(id)sender {
    if (![self inputIsLegal]) {
        return;
    }
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"farmworkTypeId"] = self.commonOperationContent.farmworkTypeId;
    NSTimeInterval timeInterval = [self.date timeIntervalSince1970];
    parameters[@"operateTime"] = @(timeInterval);
    parameters[@"images"] = [self imageParameter] ? : @"";
    parameters[@"plantArchives"] = [self areaParameter] ? : @"";
    parameters[@"agriculturalProduct"] = [self agricultureParameter] ? :@"";
    parameters[@"remarks"] = self.markTextView.text;
    [[RKNetworkHelper shareManager] POST:kAddFarming parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            [MBProgressHUD showMessage:@"添加成功"];
            [self leftBarButtonItemAction];
        } else {
            [MBProgressHUD showMessage:baseResponse.msg];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"添加失败"];
    }];
}

- (void)editFarming {
    if (![self inputIsLegal]) {
        return;
    }
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    if (!kStringIsEmpty(self.commonOperationContent.farmworkTypeId)) {
        parameters[@"farmworkTypeId"] = self.commonOperationContent.farmworkTypeId;
    }
    NSTimeInterval timeInterval = [self.date timeIntervalSince1970];
    parameters[@"operateTime"] = @(timeInterval);
    parameters[@"images"] = [self imageParameter] ? : @"";
    parameters[@"plantArchives"] = [self areaParameter] ? : @"";
    parameters[@"agriculturalProduct"] = [self agricultureParameter] ? : @"";
    parameters[@"remarks"] = self.markTextView.text;
    if (self.farmingDetail) {
        parameters[@"farmworkId"] = self.farmingDetail.farmworkId;
    }
    [[RKNetworkHelper shareManager] POST:kEditFarming parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            [MBProgressHUD showMessage:@"修改成功"];
            [self leftBarButtonItemAction];
        } else {
            [MBProgressHUD showMessage:baseResponse.msg];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"添加失败"];
    }];
}

- (NSString *)imageParameter {
    NSInteger imagesCount = self.pictureArray.count;
    NSMutableArray *imageTempArray = [NSMutableArray new];
    for (NSInteger i = 0 ; i < imagesCount; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        NSData *data = UIImageJPEGRepresentation(self.pictureArray[i], 1.0);
        NSString *str = [data base64EncodedString];
        dic[@"image"] = str;
        [imageTempArray addObject:dic];
    }
    return [imageTempArray jsonPrettyStringEncoded];
}

- (NSString *)areaParameter {
    NSInteger areaCount = self.areaArray.count;
    NSMutableArray *areaTempArray = [NSMutableArray new];
    for (NSInteger i = 0; i < areaCount; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        dic[@"plantArchivesId"] = self.areaArray[i].plant.plantArchivesId;
        [areaTempArray addObject:dic];
    }
    return [areaTempArray jsonPrettyStringEncoded];
}

- (NSString *)agricultureParameter {
    NSInteger agricultureCount = self.agricultureArray.count;
    NSMutableArray *agricultureTempArray = [NSMutableArray new];
    for (NSInteger i = 0; i < agricultureCount; i++) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        dic[@"agriculturalProductId"] = self.agricultureArray[i].agriculturalProductId;
        dic[@"agriculturalProductYield"] = self.agricultureArray[i].agriculturalProductYield;
        [agricultureTempArray addObject:dic];
    }
    return [agricultureTempArray jsonPrettyStringEncoded];
}

- (BOOL)inputIsLegal {
    if (kStringIsEmpty(self.operationContent.text)) {
        [MBProgressHUD showMessage:@"请选择操作内容"];
        return NO;
    }

    if (self.areaArray.count == 0) {
        [MBProgressHUD showMessage:@"请选择操作区域"];
        return NO;
    }

    if (kStringIsEmpty(self.operationTime.text)) {
        [MBProgressHUD showMessage:@"请选择操作时间"];
        return NO;
    }

    if (self.pictureArray.count == 0) {
        [MBProgressHUD showMessage:@"请添加农事图片"];
        return NO;
    }
    return YES;
}

#pragma mark - OperationContentViewControllerDelegate

-(void)chooseOperationContentWithContent:(CommonOperationContent *)content type:(OperationContentType)type{
    self.operationContent.text = content.farmworkTypeName;
    self.commonOperationContent = content;
    self.type = type;
}

#pragma mark - PrivateMethod

- (void)reloadAreaTableView {
    CGFloat height = 44 * self.areaArray.count;
    self.areaTableViewHeightConstraint.constant = height;
    [self.view layoutIfNeeded];
    [self.areaTableView reloadData];
}

- (void)reloadAgricultrualTableView {
    CGFloat height = 44 * self.agricultureArray.count;
    self.agriculturalTableViewHeightConstraint.constant = height;
    [self.view layoutIfNeeded];
    [self.agriculturalTableView reloadData];
}

- (void)searchOperationContent:(NSNotification *)notification {
    CommonOperationContent *operationContent = (CommonOperationContent *)notification.object;
    self.operationContent.text = operationContent.farmworkTypeName;
    self.commonOperationContent = operationContent;
}

- (void)frendlyData {
    if (self.markCount == 0 && self.enterType != EnterViewControllerTypeAddFarming) {
        [self.partitionArray enumerateObjectsUsingBlock:^(Partition * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.subarea enumerateObjectsUsingBlock:^(Subarea * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj.plants enumerateObjectsUsingBlock:^(Plant * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [self.areaArray enumerateObjectsUsingBlock:^(Area * area, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([area.plant.plantArchivesId isEqualToString:obj.plantArchivesId]) {
                            obj.selected = YES;
                        }
                        self.markCount = 1;
                    }];
                }];
            }];
        }];
    }
}
#pragma mark - Getter
- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy/MM/dd HH:mm";
    }
    return _dateFormatter;
}

- (ChooseAreaView *)chooseAreaView {
    if (!_chooseAreaView) {
        _chooseAreaView = [[ChooseAreaView alloc] initWithView:self.view areaBlock:^(Area *area) {
            [self.areaArray addObject:area];
            self.areaLogical.dataArray = self.areaArray;
            [self reloadAreaTableView];
        } cancelAreaBlock:^(Area *area) {
            NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:self.areaArray];
            [tempArray enumerateObjectsUsingBlock:^(Area *tempArea, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([area.plant.plantArchivesId isEqualToString: tempArea.plant.plantArchivesId]) {
                    [self.areaArray removeObjectAtIndex:idx];
                    *stop = YES;
                }
            }];
            self.areaLogical.dataArray = self.areaArray;
            [self reloadAreaTableView];
        }];
    }
    return _chooseAreaView;
}

- (NSMutableArray *)areaArray {
    if (!_areaArray) {
        _areaArray = [[NSMutableArray alloc] init];
    }
    return _areaArray;
}

- (NSArray *)pictureArray {
    if (!_pictureArray) {
        _pictureArray = [[NSArray alloc] init];
    }
    return _pictureArray;
}

- (NSMutableArray *)agricultureArray {
    if (!_agricultureArray) {
        _agricultureArray = [[NSMutableArray alloc] init];
    }
    return _agricultureArray;
}


@end
