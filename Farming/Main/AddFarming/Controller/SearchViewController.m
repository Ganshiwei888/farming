//
//  SearchViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "SearchViewController.h"
#import "JCTagListView.h"
#import "CommonOperationContent.h"

@interface SearchViewController () <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet JCTagListView *listView;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSArray<CommonOperationContent *> *dataArray;
@property (nonatomic, strong) NSMutableArray *chooseContents;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
}

- (void)setupNavigation {

    self.searchBar =[[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 2 * 44 - 2 * 15, 44)];
    self.searchBar.placeholder = @"请输入搜索内容";
    self.searchBar.tintColor = kGreenColor;
    self.searchBar.delegate = self;
    if ([self.searchBar canBecomeFirstResponder]) {
        [self.searchBar becomeFirstResponder];
    }
    UIView *wrapView = [[UIView alloc] initWithFrame:self.searchBar.frame];
    [wrapView addSubview:self.searchBar];
    self.navigationItem.titleView = wrapView;
    self.rightBarButtonItemTitle = @"取消";

}

- (void)rightBarButtonItemAction {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"farmworkTypeName"] = searchBar.text;
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kOperationContent parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.dataArray = [CommonOperationContent mj_objectArrayWithKeyValuesArray:baseResponse.data];
                [self setupViews];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (void)setupViews {
    NSInteger chooseCount = self.dataArray.count;
    for (NSInteger i = 0; i < chooseCount; i ++) {
        CommonOperationContent *content = self.dataArray[i];
        [self.chooseContents addObject: content.farmworkTypeName];
    }

    [self.listView setTags:self.chooseContents];
    [self.listView setCompletionBlockWithSelected:^(NSInteger index) {
        CommonOperationContent *content = self.dataArray[index];
        content.date = [NSDate date];
        [Util handleCommonOperation:content];
        [kNotificationCenter postNotificationName:kSearchOperationContent object:content];
        NSArray<UIViewController *> *viewControllers = self.navigationController.viewControllers;
        NSInteger count = viewControllers.count - 1;
        [self.navigationController popToViewController:viewControllers[count - 2] animated:YES];
    }];
    [self.listView.collectionView reloadData];
}

- (NSMutableArray *)chooseContents {
    if (!_chooseContents) {
        _chooseContents = [NSMutableArray new];
    }
    return _chooseContents;
}

@end
