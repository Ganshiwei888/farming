//
//  AddFarmingViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/14.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseViewController.h"

@interface AddFarmingViewController : BaseViewController

@property (nonatomic, assign) EnterViewControllerType enterType;
@property (nonatomic, copy) NSString *farmworkId;

@end
