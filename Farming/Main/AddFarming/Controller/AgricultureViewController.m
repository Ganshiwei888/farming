//
//  AgricultureViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AgricultureViewController.h"
#import "AgriculturePage.h"
#import "SearchAgriculturalViewController.h"
#import <MJRefresh.h>

@interface AgricultureViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<Agriculture *> *dataArray;
@property (strong, nonatomic) AgriculturePage *agriculturePage;
@property (assign, nonatomic) NSInteger page;

@end

@implementation AgricultureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择农资";
    self.page = 1;
    self.rightBarButtonItemImage = [UIImage imageNamed:@"search"];
    [self setupDataWithPage:self.page];
    [self setupRefresh];
    self.tableView.tableFooterView = [UIView new];
}

- (void)setupDataWithPage:(NSInteger)page {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"agriculturalProductCategory"] = self.agricultureCategory;
    parameters[@"page"] = @(page);
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kAgricultrue parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.agriculturePage = [AgriculturePage mj_objectWithKeyValues:baseResponse.data];
            if (page == 1) {
                self.dataArray = self.agriculturePage.agricultureList;
            } else {
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                [tempArray addObjectsFromArray:self.agriculturePage.agricultureList];
                self.dataArray = tempArray;
            }
            [self.tableView reloadData];
        }
        [self endRefresh];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefresh];
    }];
}

- (void)setupRefresh {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self setupDataWithPage:self.page];
    }];

    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self setupDataWithPage:self.page];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];

    if (self.agriculturePage.current_page < self.agriculturePage.last_page) {
        [self.tableView.mj_footer endRefreshing];
    } else {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)rightBarButtonItemAction {
    SearchAgriculturalViewController *searchAgricutureViewController = [[SearchAgriculturalViewController alloc] init];
    searchAgricutureViewController.agricultureCategory = self.agricultureCategory;
    [self.navigationController pushViewController:searchAgricutureViewController animated:YES];
    
}

#pragma mark - UITableViewDataSource 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    Agriculture *agriculture = self.dataArray[indexPath.row];
    cell.textLabel.text = agriculture.agriculturalProductName;
    cell.textLabel.textColor = kFontBlackColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Agriculture *agriculture = self.dataArray[indexPath.row];
    if (self.chooseAgricultureBlock) {
        self.chooseAgricultureBlock(agriculture);
        [self backAction];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}


@end
