//
//  OperationContentViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseViewController.h"

@class CommonOperationContent;

typedef NS_ENUM(NSInteger, OperationContentType) {
    OperationContentTypeChoose,
    OperationContentTypeCommon
};

@protocol OperationContentViewControllerDelegate <NSObject>

- (void)chooseOperationContentWithContent:(CommonOperationContent *)content type:(OperationContentType)type;

@end

@interface OperationContentViewController : BaseViewController

@property (nonatomic, copy) NSString *contentStr;
@property (nonatomic, assign) OperationContentType type;
@property (nonatomic, weak) id<OperationContentViewControllerDelegate> delegate;

@end
