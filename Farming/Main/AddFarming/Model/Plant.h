//
//  Plant.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Plant : NSObject

@property (nonatomic, copy) NSString * plantArchivesId;
@property (nonatomic, copy) NSString * productOptionId;
@property (nonatomic, copy) NSString * productOptionName;
@property (nonatomic, copy) NSString * productName;
@property (nonatomic, assign) NSTimeInterval startTime;
@property (nonatomic, assign) NSTimeInterval endTime;
@property (nonatomic, copy) NSString * image;

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *cateId;

@property (nonatomic, assign) BOOL selected;

@end
