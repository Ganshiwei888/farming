//
//  Agriculture.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Agriculture : NSObject

@property (nonatomic, copy) NSString *agriculturalProductName;
@property (nonatomic, copy) NSString *agriculturalProductId;
@property (nonatomic, copy) NSString *agriculturalProductCategory;
@property (nonatomic, copy) NSString *agriculturalProductYield;
@property (nonatomic, copy) NSString *unit;

@end
