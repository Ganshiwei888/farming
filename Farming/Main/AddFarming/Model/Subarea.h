//
//  Subarea.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Plant.h"

@interface Subarea : NSObject

@property (nonatomic, copy) NSString * subareaId;
@property (nonatomic, copy) NSString * subareaName;
@property (nonatomic, assign) CGFloat subareaAcreage;
@property (nonatomic, strong) NSArray<Plant *> * plants;

@property (nonatomic, assign) BOOL selected;

@end
