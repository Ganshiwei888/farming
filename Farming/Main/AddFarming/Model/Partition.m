//
//  Partition.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "Partition.h"

@implementation Partition

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"subarea" : @"Subarea"};
}

@end
