//
//  FarmingDetail.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/4.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "FarmingDetail.h"
#import "Partition.h"
#import "Area.h"
#import "SubsidiaryArea.h"
#import "Agriculture.h"
#import "CommonOperationContent.h"

@implementation FarmingDetail

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"plantArchives" : @"SubsidiaryArea",
             @"agriculturalProduct" : @"Agriculture"};
}


+ (instancetype)initWithDic:(NSDictionary *)dic {
    FarmingDetail *detail = [FarmingDetail mj_objectWithKeyValues:dic];
    [detail dataFriendly];
    return detail;
}

- (void)dataFriendly {
    self.operationContent = [[CommonOperationContent alloc] init];
    self.operationContent.farmworkTypeName = self.typeName;
    self.operationContent.farmworkTypeId = self.farmworkTypeId;

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.operateTime];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd HH:mm";
    self.operationTimeStr = [dateFormatter stringFromDate:date];

    self.areas = [NSMutableArray new];
    NSInteger count = self.plantArchives.count;
    for (NSInteger i = 0; i < count; i ++) {
        SubsidiaryArea *subsidiaryArea = self.plantArchives[i];
        Area *area = [[Area alloc] init];
        Partition *partition = [Partition new];
        partition.partitionName =subsidiaryArea.partitionName;
        area.partition = partition;

        Subarea *subarea = [Subarea new];
        subarea.subareaName = subsidiaryArea.subareaName;
        area.subarea = subarea;

        Plant *plant = [Plant new];
        plant.productOptionName = subsidiaryArea.productOptionName;
        plant.plantArchivesId = subsidiaryArea.plantArchivesId;
        plant.productOptionId = subsidiaryArea.productId;
        area.plant = plant;

        [self.areas addObject:area];
    }

}

@end
