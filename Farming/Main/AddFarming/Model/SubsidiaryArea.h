//
//  SubsidiaryArea.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/4.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubsidiaryArea : NSObject

@property (nonatomic, copy) NSString * partitionName;
@property (nonatomic, copy) NSString * subareaName;
@property (nonatomic, copy) NSString * productOptionName;
@property (nonatomic, copy) NSString * plantArchivesId;
@property (nonatomic, copy) NSString * productId;

@end
