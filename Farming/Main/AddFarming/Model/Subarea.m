//
//  Subarea.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "Subarea.h"

@implementation Subarea

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"plants" : @"Plant"};
}

@end
