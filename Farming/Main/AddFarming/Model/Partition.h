//
//  Partition.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Subarea.h"

@interface Partition : NSObject

@property (nonatomic, copy) NSString * partitionId;
@property (nonatomic, copy) NSString * partitionName;
@property (nonatomic, strong) NSArray<Subarea *> * subarea;

@property (nonatomic, assign) BOOL selected;

@end
