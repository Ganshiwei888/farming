//
//  FarmingDetail.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/4.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SubsidiaryArea;
@class Agriculture;
@class CommonOperationContent;
@class Area;

@interface FarmingDetail : NSObject

@property (nonatomic, copy) NSString * farmworkId;
@property (nonatomic, copy) NSString * farmworkTypeId;
@property (nonatomic, copy) NSString * typeName;
@property (nonatomic, strong) NSArray *image;
@property (nonatomic, assign) NSTimeInterval operateTime;
@property (nonatomic, strong) NSArray<SubsidiaryArea *> *plantArchives;
@property (nonatomic, strong) NSArray<Agriculture *> *agriculturalProduct;
@property (nonatomic, copy) NSString *remarks;
@property (nonatomic, copy) NSString *operationTimeStr;
@property (nonatomic, strong) CommonOperationContent *operationContent;
@property (nonatomic, strong) NSMutableArray<Area *> *areas;

+ (instancetype)initWithDic:(NSDictionary *)dic;

@end
