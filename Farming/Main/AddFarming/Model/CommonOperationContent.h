//
//  CommonOperationContent.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/21.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Realm/Realm.h>

@interface CommonOperationContent : RLMObject

@property (nonatomic, copy) NSString *farmworkTypeId;
@property (nonatomic, copy) NSString *farmworkTypeName;
@property (nonatomic, strong) NSDate *date;

@end
