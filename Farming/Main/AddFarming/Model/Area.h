//
//  Area.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Partition;
@class Subarea;
@class Plant;

@interface Area : NSObject

@property (nonatomic, strong) Partition *partition;
@property (nonatomic, strong) Subarea *subarea;
@property (nonatomic, strong) Plant *plant;

@end
