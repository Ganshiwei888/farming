//
//  AgricultureCategory.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/1.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgricultureCategory : NSObject

@property (nonatomic, copy) NSString *chineseName;
@property (nonatomic, copy) NSString *englishName;

@end
