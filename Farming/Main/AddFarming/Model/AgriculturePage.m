//
//  AgriculturePage.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/25.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AgriculturePage.h"

@implementation AgriculturePage

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"agricultureList" : @"Agriculture"};
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"agricultureList" : @"data"};
}

@end
