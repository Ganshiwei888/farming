//
//  AgricultrueLogical.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DeleteBlock)(NSInteger row);
typedef NS_ENUM(NSInteger, TableViewType) {
    AreaTableViewType,
    AgricultureTableViewType
};


@interface AgricultrueLogical : NSObject <UITableViewDelegate, UITableViewDataSource>

- (instancetype)initWithTableView:(UITableView *)tableView tableViewType:(TableViewType)tableViewType deleteBlock:(DeleteBlock)deleteBlock;

@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, assign) EnterViewControllerType enterType;

@end
