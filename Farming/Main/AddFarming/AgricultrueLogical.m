//
//  AgricultrueLogical.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/22.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AgricultrueLogical.h"
#import "AgricultrueCell.h"
#import "AreaCell.h"
#import "Agriculture.h"
#import "Area.h"

static NSString *const agricultrueCellIdentifier = @"AgricultrueCell";
static NSString *const areaCellIdentifier = @"AreaCell";

@interface AgricultrueLogical () <AgricultrueCellDelegate, AreaCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) DeleteBlock deleteBlock;
@property (nonatomic, assign) TableViewType tableViewType;

@end

@implementation AgricultrueLogical

- (instancetype)initWithTableView:(UITableView *)tableView tableViewType:(TableViewType)tableViewType deleteBlock:(DeleteBlock)deleteBlock{
    if (self = [super init]) {
        _tableView = tableView;
        _deleteBlock = deleteBlock;
        _tableViewType = tableViewType;
        [self setupTableView];
    }
    return self;
}

- (void)setupTableView {
    switch (self.tableViewType) {
        case AreaTableViewType:
            [self.tableView registerNib:[AreaCell defaultNib] forCellReuseIdentifier:areaCellIdentifier];
            break;
        case AgricultureTableViewType:
                [self.tableView registerNib:[AgricultrueCell defaultNib] forCellReuseIdentifier:agricultrueCellIdentifier];
        default:
            break;
    }

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tableViewType == AgricultureTableViewType) {
        Agriculture *agriculture = self.dataArray[indexPath.row];
        AgricultrueCell *cell = [tableView dequeueReusableCellWithIdentifier:agricultrueCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        [cell reload:agriculture enterType:self.enterType];
        return cell;
    } else {
        Area *area = self.dataArray[indexPath.row];
        AreaCell *cell = [tableView dequeueReusableCellWithIdentifier:areaCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        [cell reload:area enterType:self.enterType];
        return cell;
    }

}

#pragma mark - AgricultrueCellDelegate

- (void)deleteWithCell:(AgricultrueCell *)cell {
    NSIndexPath *indexpath = [self.tableView indexPathForCell:cell];
    if (self.deleteBlock) {
        self.deleteBlock(indexpath.row);
    }
}

#pragma mark - AreaCellDelegate 

- (void)deleteAreaWithCell:(AreaCell *)cell {
    NSIndexPath *indexpath = [self.tableView indexPathForCell:cell];
    if (self.deleteBlock) {
        self.deleteBlock(indexpath.row);
    }
}

#pragma mark - Setter

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

- (void)setEnterType:(EnterViewControllerType)enterType {
    _enterType = enterType;
    [self.tableView reloadData];
}

@end
