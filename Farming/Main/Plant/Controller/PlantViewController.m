//
//  PlantViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/14.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "PlantViewController.h"
#import "PlantCell.h"
#import "PlantHeaderView.h"
#import "CropViewController.h"
#import "Partition.h"
#import <MJRefresh.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

static NSString *const plantCellIdentifier = @"PlantCell";
static NSString *const plantHeaderViewIdentifier = @"PlantHeaderView";

@interface PlantViewController () <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<Partition *> *dataArray;

@end

@implementation PlantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupData];
    [self setupRefresh];
}

- (void)setupTableView {
    [self.tableView registerNib:[PlantCell defaultNib] forCellReuseIdentifier:plantCellIdentifier];
    [self.tableView registerNib:[PlantHeaderView defaultNib] forHeaderFooterViewReuseIdentifier:plantHeaderViewIdentifier];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = [UIView new];
}

- (void)setupData {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kPlantIndex parameters:nil success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.dataArray = [Partition mj_objectArrayWithKeyValuesArray:baseResponse.data];
            [self.tableView reloadData];
        }
        [self endRefresh];
    } failure:^(NSError *error) {
        [self endRefresh];
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (void)setupRefresh {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self setupData];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Partition *partition = self.dataArray[section];
    if (partition.selected) {
        return partition.subarea.count;
    } else {
        return 0;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlantCell *cell = [tableView dequeueReusableCellWithIdentifier:plantCellIdentifier forIndexPath:indexPath];
    Subarea *subarea = self.dataArray[indexPath.section].subarea[indexPath.row];
    cell.area.text = subarea.subareaName;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PlantHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:plantHeaderViewIdentifier];
    [headerView setClickBlock:^(NSInteger section){
        [tableView reloadSection:section withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    Partition *partition = self.dataArray[section];
    [headerView reloadWithPartition:partition section:section];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 37.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CropViewController *cropViewController = [[CropViewController alloc] init];
    Subarea *subarea = self.dataArray[indexPath.section].subarea[indexPath.row];
    cropViewController.subarea = subarea;
    [self.navigationController pushViewController:cropViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - DZNEmptyDataSetSource

//- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
//
//    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"下拉刷新" attributes:@{NSForegroundColorAttributeName: kFontGrayColor, NSFontAttributeName:kSystemFont(17.0)}];
//    return str;
//}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    [self setupData];
}

@end
