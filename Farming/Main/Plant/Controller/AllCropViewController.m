//
//  AllCropViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/5.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AllCropViewController.h"
#import "PlantPage.h"
#import "SearchCropViewController.h"
#import <MJRefresh.h>

@interface AllCropViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray<Plant *> *dataArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) PlantPage *plantPage;
@property (assign, nonatomic) NSInteger page;

@end

@implementation AllCropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"作物";
    self.page = 1;
    [self setupNavigation];
    [self setupDataWithPage:self.page];
    [self setupRefresh];
    self.tableView.tableFooterView = [UIView new];
    if (@available(ios 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)setupNavigation {
    self.rightBarButtonItemImage = [UIImage imageNamed:@"search"];
}

- (void)rightBarButtonItemAction {
    SearchCropViewController *searchCropViewController = [[SearchCropViewController alloc] init];
    [self.navigationController pushViewController:searchCropViewController animated:YES];
}

- (void)setupDataWithPage:(NSInteger)page {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"page"] = @(page);
    [[RKNetworkHelper shareManager] POST:kAllCrop parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.plantPage = [PlantPage mj_objectWithKeyValues:baseResponse.data];
            if (page == 1) {
                self.dataArray = self.plantPage.plantList;
            } else {
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                [tempArray addObjectsFromArray:self.plantPage.plantList];
                self.dataArray = tempArray;
            }
            [self.tableView reloadData];
        }
        [self endRefresh];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefresh];
    }];
}

- (void)setupRefresh {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self setupDataWithPage:self.page];
    }];

    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self setupDataWithPage:self.page];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];

    if (self.plantPage.current_page < self.plantPage.last_page) {
        [self.tableView.mj_footer endRefreshing];
    } else {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    Plant *plant = self.dataArray[indexPath.row];
    cell.textLabel.text = plant.productOptionName;
    cell.textLabel.textColor = kFontBlackColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Plant *plant = self.dataArray[indexPath.row];
    if (self.choosePlantBlock) {
        self.choosePlantBlock(plant);
        [self backAction];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
