
//
//  CropViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/15.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropViewController.h"
#import "CropGrowthCycleCell.h"
#import "AddPlantViewController.h"
#import "CropDetailViewController.h"
#import "Subarea.h"
#import <MJRefresh.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "PlantPage.h"

static NSString *const cropGrowthCycleCellIdentifier = @"CropGrowthCycleCell";

@interface CropViewController () <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<Plant *> *dataArray;
@property (strong, nonatomic) PlantPage *plantPage;
@property (assign, nonatomic) NSInteger page;

@end

@implementation CropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.subarea.subareaName;
    self.page = 1;
    [self setupTableView];
    [self setupDataWithPage:self.page];
    [self setupRefresh];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[Util drawLinearGradientWith:self.navigationController.navigationBar.frame] forBarMetrics:UIBarMetricsDefault];
}

- (void)setupTableView {
    [self.tableView registerNib:[CropGrowthCycleCell defaultNib] forCellReuseIdentifier:cropGrowthCycleCellIdentifier];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 156;
}

- (void)setupDataWithPage:(NSInteger)page {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"subareaId"] = self.subarea.subareaId;
    parameters[@"page"] = @(page);
    [[RKNetworkHelper shareManager] POST:kPlantCrop parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.plantPage = [PlantPage mj_objectWithKeyValues:baseResponse.data];
            if (page == 1) {
                self.dataArray = self.plantPage.plantList;
            } else {
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                [tempArray addObjectsFromArray:self.plantPage.plantList];
                self.dataArray = tempArray;
            }
            [self.tableView reloadData];
        }
        [self endRefresh];
    } failure:^(NSError *error) {
        [self endRefresh];
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (void)setupRefresh {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self setupDataWithPage:self.page];
    }];

    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self setupDataWithPage:self.page];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];

    if (self.plantPage.current_page < self.plantPage.last_page) {
        [self.tableView.mj_footer endRefreshing];
    } else {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CropGrowthCycleCell *cell = [tableView dequeueReusableCellWithIdentifier:cropGrowthCycleCellIdentifier forIndexPath:indexPath];
    Plant *plant = self.dataArray[indexPath.row];
    [cell reloadWithPlant:plant];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CropDetailViewController *cropDetailViewController = [[CropDetailViewController alloc] init];
    Plant *plant = self.dataArray[indexPath.row];
    cropDetailViewController.plant = plant;
    cropDetailViewController.subareaName = self.subarea.subareaName;
    [self.navigationController pushViewController:cropDetailViewController animated:YES];
}

- (IBAction)addPlant:(UIButton *)sender {
    AddPlantViewController *addPlantViewController = [[AddPlantViewController alloc] init];
    addPlantViewController.subarea = self.subarea;
    [self.navigationController pushViewController:addPlantViewController animated:YES];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {

    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"区域内暂无种植作物" attributes:@{NSForegroundColorAttributeName: kFontGrayColor, NSFontAttributeName:kSystemFont(17.0)}];
    return str;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    self.page = 1;
    [self setupDataWithPage:self.page];
}

@end
