//
//  AddPlantViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseViewController.h"

@class Subarea;

@interface AddPlantViewController : BaseViewController

@property (nonatomic, strong) Subarea *subarea;

@end
