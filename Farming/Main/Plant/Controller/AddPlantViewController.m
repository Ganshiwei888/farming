//
//  AddPlantViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AddPlantViewController.h"
#import "Subarea.h"
#import "AllCropViewController.h"
#import "Plant.h"

static CGFloat const dataPickerHeight = 150;

@interface AddPlantViewController ()

@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *cropName;
@property (weak, nonatomic) IBOutlet UILabel *plantTime;
@property (weak, nonatomic) IBOutlet UILabel *harvestStartTime;
@property (weak, nonatomic) IBOutlet UILabel *harvestOverTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *plantDatePickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *harvestStartDatePickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *harvestOverDatePickerHeightConstraint;

@property (weak, nonatomic) IBOutlet UIDatePicker *plantDatePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *harvestStartDatePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *harvestOverDatePicker;
@property (weak, nonatomic) IBOutlet UITextField *plantArea;
@property (weak, nonatomic) IBOutlet UILabel *totalArea;
@property (weak, nonatomic) IBOutlet UILabel *warningLabel;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSDate *plantDate;
@property (strong, nonatomic) NSDate *harvestStartDate;
@property (strong, nonatomic) NSDate *harvestOverDate;

@property (strong, nonatomic) Plant *plant;

@end

@implementation AddPlantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.subarea.subareaName;
    [self setupDatePicker];
    [self setupData];
    [self addObserver];
}

- (void)dealloc {
    [kNotificationCenter removeObserver:self];
}

- (void)setupDatePicker {
    self.plantDatePicker.minimumDate = [NSDate date];
    self.harvestStartDatePicker.minimumDate = [NSDate date];
    self.harvestOverDatePicker.minimumDate = [NSDate date];
}

- (void)setupData {
    self.areaLabel.text = self.subarea.subareaName;
    self.totalArea.text = [NSString stringWithFormat:@"%.2f",self.subarea.subareaAcreage];
    self.warningLabel.hidden = YES;
}

- (void)addObserver {
    [kNotificationCenter addObserver:self selector:@selector(plantAreaChange:) name:UITextFieldTextDidChangeNotification object:self.plantArea];
    [kNotificationCenter addObserver:self selector:@selector(searchCrop:) name:kSearchCrop object:nil];
}

- (void)searchCrop:(NSNotification *)notification {
    Plant *plant = (Plant *)notification.object;
    self.plant = plant;
    self.cropName.text = plant.productOptionName;
}

- (void)plantAreaChange:(NSNotification *)notification {
    CGFloat inputPlantArea = self.plantArea.text.floatValue;
    if (inputPlantArea > self.subarea.subareaAcreage) {
        self.warningLabel.hidden = NO;
        self.plantArea.text = @"";
    } else {
        self.warningLabel.hidden = YES;
    }
}

- (IBAction)chooseCrop:(UIButton *)sender {
    AllCropViewController *allCropViewController = [[AllCropViewController alloc] init];
    [allCropViewController setChoosePlantBlock:^(Plant *plant){
        self.plant = plant;
        self.cropName.text = plant.productOptionName;
    }];
    [self.navigationController pushViewController:allCropViewController animated:YES];
}

- (IBAction)choosePlantTime:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.plantDatePicker.hidden = NO;
        self.plantDatePickerHeightConstraint.constant = dataPickerHeight;
    } else {
        self.plantDatePicker.hidden = YES;
        self.plantDatePickerHeightConstraint.constant = 0;
    }
    [self.view layoutIfNeeded];

}

- (IBAction)chooseHarvestStartTime:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.harvestStartDatePicker.hidden = NO;
        self.harvestStartDatePickerHeightConstraint.constant = dataPickerHeight;
    } else {
        self.harvestStartDatePicker.hidden = YES;
        self.harvestStartDatePickerHeightConstraint.constant = 0;
    }
    [self.view layoutIfNeeded];
}

- (IBAction)chooseHarvestOverTime:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.harvestOverDatePicker.hidden = NO;
        self.harvestOverDatePickerHeightConstraint.constant = dataPickerHeight;
    } else {
        self.harvestOverDatePicker.hidden = YES;
        self.harvestOverDatePickerHeightConstraint.constant = 0;
    }
    [self.view layoutIfNeeded];
}

- (IBAction)plantDatePicker:(UIDatePicker *)sender {
    NSDate *date = sender.date;
    self.plantTime.text = [self.dateFormatter stringFromDate:date];
    self.plantDate = date;
    [self.harvestStartDatePicker setMinimumDate:self.plantDate];
}

- (IBAction)harvestStartDatePicker:(UIDatePicker *)sender {
    NSDate *date = sender.date;
    self.harvestStartTime.text = [self.dateFormatter stringFromDate:date];
    self.harvestStartDate = date;
    [self.harvestOverDatePicker setMinimumDate:self.harvestStartDate];
}

- (IBAction)harvestOverDatePicker:(UIDatePicker *)sender {
    NSDate *date = sender.date;
    self.harvestOverTime.text = [self.dateFormatter stringFromDate:date];
    self.harvestOverDate = date;

}

- (IBAction)commit:(id)sender {

    if (![self inputIsLegal]) {
        return;
    }

    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"subareaId"] = self.subarea.subareaId;
    parameters[@"productId"] = self.plant.productId;
    parameters[@"productOptionId"] = self.plant.productOptionId;
    parameters[@"plantAcreage"] = self.plantArea.text;
    parameters[@"startTime"] = @([self.plantDate timeIntervalSince1970]);
    parameters[@"takeTime"] = @([self.harvestStartDate timeIntervalSince1970]);
    parameters[@"endTime"] = @([self.harvestOverDate timeIntervalSince1970]);

    [[RKNetworkHelper shareManager] POST:kAddCrop parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            [MBProgressHUD showMessage:@"添加成功"];
            [self backAction];
        } else {
            [MBProgressHUD showMessage:baseResponse.msg];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"添加失败"];
    }];

}

- (BOOL)inputIsLegal {
    if (kStringIsEmpty(self.cropName.text)) {
        [MBProgressHUD showMessage:@"请选择作物"];
        return NO;
    }

    if (kStringIsEmpty(self.plantArea.text)) {
        [MBProgressHUD showMessage:@"请输入种植面积"];
        return NO;
    }

    if (kStringIsEmpty(self.plantTime.text)) {
        [MBProgressHUD showMessage:@"请选择种植时间"];
        return NO;
    }


    if (kStringIsEmpty(self.harvestStartTime.text)) {
        [MBProgressHUD showMessage:@"请选择采收开始时间"];
        return NO;
    }

    if (kStringIsEmpty(self.harvestOverTime.text)) {
        [MBProgressHUD showMessage:@"请选择采收结束时间"];
        return NO;
    }

    if ([self.plantDate compare:self.harvestStartDate] == NSOrderedDescending) {
        [MBProgressHUD showMessage:@"采收开始时间不能早于种植时间"];
        return NO;
    }

    if ([self.harvestStartDate compare:self.harvestOverDate] == NSOrderedDescending) {
        [MBProgressHUD showMessage:@"采收结束时间不能早于采收开始时间"];
        return NO;
    }
    return YES;
}

#pragma mark - Getter

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy/MM/dd";
    }
    return _dateFormatter;
}

@end
