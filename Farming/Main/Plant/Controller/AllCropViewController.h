//
//  AllCropViewController.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/5.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseViewController.h"

@class Plant;
typedef void(^ChoosePlantBlock)(Plant *plant);

@interface AllCropViewController : BaseViewController

@property (nonatomic, strong) ChoosePlantBlock choosePlantBlock;

@end
