//
//  CropDetailViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropDetailViewController.h"
#import "CropDetailHeaderView.h"
#import "CropDetailCell.h"
#import "AddFarmingViewController.h"
#import "Plant.h"
#import "CropDetail.h"

static NSString *const cropDetailCellIdentifier = @"CropDetailCell";

@interface CropDetailViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) CropDetail *cropDetail;

@end

@implementation CropDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.plant.productOptionName;
    [self setupTableView];
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIImage *image = [UIImage imageWithColor:kGreenColor];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
}

- (void)setupTableView {
    [self.tableView registerNib:[CropDetailCell defaultNib] forCellReuseIdentifier:cropDetailCellIdentifier];
    self.tableView.rowHeight = 113;
    self.tableView.sectionHeaderHeight = 152;
}

- (void)setupData {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"plantArchivesId"] = self.plant.plantArchivesId;
    [[RKNetworkHelper shareManager] POST:kCropDetail parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        self.cropDetail = [CropDetail mj_objectWithKeyValues:baseResponse.data];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
    }];
}

- (IBAction)addFarmingAction:(UIButton *)sender {
    AddFarmingViewController *addFarmingViewController = [[AddFarmingViewController alloc] init];
    [self.navigationController pushViewController:addFarmingViewController animated:YES];
}

#pragma mark - UITableViewDataSource 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cropDetail.farmwork.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CropDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cropDetailCellIdentifier forIndexPath:indexPath];
    SubsidiaryCropDetail *sub = self.cropDetail.farmwork[indexPath.row];
    [cell reloadWithSubsidiaryCropDetail:sub];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CropDetailHeaderView *view = [[NSBundle mainBundle] loadNibNamed:@"CropDetailHeaderView" owner:self options:nil][0];
    if (self.cropDetail) {
        [view reloadWithCropDetail:self.cropDetail];
    }
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddFarmingViewController *addFarmingViewController = [[AddFarmingViewController alloc] init];
    SubsidiaryCropDetail *sub = self.cropDetail.farmwork[indexPath.row];
    addFarmingViewController.farmworkId = sub.farmworkId;
    addFarmingViewController.enterType = EnterViewControllerTypeCropDetail;
    [self.navigationController pushViewController:addFarmingViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
