//
//  SubsidiaryCropDetail.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/6.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Agriculture.h"

@interface SubsidiaryCropDetail : NSObject

@property (nonatomic, copy) NSString * farmworkId;
@property (nonatomic, copy) NSString * farmworkTypeName;
@property (nonatomic, copy) NSString * image;
@property (nonatomic, assign) NSTimeInterval operateTime;
@property (nonatomic, strong) NSArray<Agriculture *> *agriculturalProduct;

@end
