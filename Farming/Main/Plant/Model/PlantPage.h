//
//  PlantPage.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/25.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Plant.h"

@interface PlantPage : NSObject

@property (nonatomic, assign) NSInteger current_page;
@property (nonatomic, assign) NSInteger last_page;
@property (nonatomic, strong) NSArray <Plant *> *plantList;

@end
