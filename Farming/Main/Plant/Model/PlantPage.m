//
//  PlantPage.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/25.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "PlantPage.h"

@implementation PlantPage

+ (NSDictionary *)mj_objectClassInArray {
    return  @{@"plantList" : @"Plant"};
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"plantList" : @"data"};
}

@end
