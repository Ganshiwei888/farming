//
//  CropDetail.h
//  Farming
//
//  Created by 甘世伟 on 2017/9/6.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubsidiaryCropDetail.h"

@interface CropDetail : NSObject

@property (nonatomic, copy) NSString * productOptionName;
@property (nonatomic, copy) NSString * subareaName;
@property (nonatomic, copy) NSString * partitionName;
@property (nonatomic, assign) NSTimeInterval startTime;
@property (nonatomic, assign) NSTimeInterval endTime;
@property (nonatomic, strong) NSArray<SubsidiaryCropDetail *> *farmwork;

@end
