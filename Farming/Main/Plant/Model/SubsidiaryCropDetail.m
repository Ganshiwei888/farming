//
//  SubsidiaryCropDetail.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/6.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "SubsidiaryCropDetail.h"

@implementation SubsidiaryCropDetail

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"agriculturalProduct" : @"Agriculture"};
}

@end
