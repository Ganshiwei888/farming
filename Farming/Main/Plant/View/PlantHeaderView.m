//
//  PlantHeaderView.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "PlantHeaderView.h"
#import "Partition.h"

@interface PlantHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UILabel *area;
@property (strong, nonatomic) Partition *partition;
@property (assign, nonatomic) NSInteger section;

@end

@implementation PlantHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = kBackgroundColor;
}

- (IBAction)click:(UIButton *)sender {
    self.partition.selected = !self.partition.selected;
    if (self.clickBlock) {
        self.clickBlock(self.section);
    }
}

- (void)reloadWithPartition:(Partition *)partittion section:(NSInteger)section{
    self.partition = partittion;
    self.section = section;
    self.area.text = partittion.partitionName;
    CGAffineTransform transform = CGAffineTransformIdentity;
    if (self.partition.selected) {
        self.arrow.transform = CGAffineTransformRotate(transform, M_PI_2);
    } else {
        self.arrow.transform = transform;
    }
}


@end
