//
//  PlantHeaderView.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Partition;

typedef void (^ClickBlock)(NSInteger SECTION);

@interface PlantHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) ClickBlock clickBlock;

- (void)reloadWithPartition:(Partition *)partittion section:(NSInteger)section;

@end
