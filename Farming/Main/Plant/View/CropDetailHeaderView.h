//
//  CropDetailHeaderView.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CropDetail;

@interface CropDetailHeaderView : UIView

- (void)reloadWithCropDetail:(CropDetail *)cropDetail;

@end
