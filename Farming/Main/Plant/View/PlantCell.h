//
//  PlantCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *area;

@end
