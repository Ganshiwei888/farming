//
//  CropDetailHeaderView.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropDetailHeaderView.h"
#import "CropDetail.h"

@interface CropDetailHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *area;

@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *lastedDay;
@property (weak, nonatomic) IBOutlet UILabel *totalDay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressRightConstraint;

@end

@implementation CropDetailHeaderView

- (void)reloadWithCropDetail:(CropDetail *)cropDetail{
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:cropDetail.startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:cropDetail.endTime];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unit = NSCalendarUnitDay;

    NSDateComponents *totalComponents = [calendar components:unit fromDate:startDate toDate:endDate options:0];
    self.totalDay.text = [NSString stringWithFormat:@"%ld",(long)totalComponents.day];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
    CGFloat last = 0.0f;
    if (timeInterval < 0) {
        self.lastedDay.text = @"0";
        last = 0;
    } else {
        NSDateComponents *lastComponents = [calendar components:unit fromDate:startDate toDate:currentDate options:0];
        self.lastedDay.text = [NSString stringWithFormat:@"%ld",(long)lastComponents.day];
        last = lastComponents.day;
    }

    self.area.text = [NSString stringWithFormat:@"%@\\%@", cropDetail.partitionName, cropDetail.subareaName];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy/MM/dd";
    self.startTime.text = [formatter stringFromDate:startDate];
    self.endTime.text = [formatter stringFromDate:endDate];

    CGFloat progress = (CGRectGetWidth(self.bounds) - 2*18) * ( (totalComponents.day-last) / totalComponents.day);
    self.progressRightConstraint.constant = progress + 18;
    [self layoutIfNeeded];
}

@end
