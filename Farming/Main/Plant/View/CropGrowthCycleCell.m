//
//  CropGrowthCycleCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropGrowthCycleCell.h"
#import "UIImage+corner.h"
#import "Plant.h"

@interface CropGrowthCycleCell ()
@property (weak, nonatomic) IBOutlet UILabel *cropName;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *lastDays;
@property (weak, nonatomic) IBOutlet UILabel *totalDays;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressRightConstraint;


@end

@implementation CropGrowthCycleCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    CGFloat padding = 16;
    frame.origin.x = padding;
    frame.size.width -= 2 * padding;
    frame.origin.y += padding;
    frame.size.height -= padding;
    [super setFrame:frame];
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(10, 10)];
    shapeLayer.path = bezierPath.CGPath;
    self.layer.mask = shapeLayer;
}

- (void)reloadWithPlant:(Plant *)plant {
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:plant.startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:plant.endTime];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unit = NSCalendarUnitDay;

    NSDateComponents *totalComponents = [calendar components:unit fromDate:startDate toDate:endDate options:0];
    self.totalDays.text = [NSString stringWithFormat:@"%ld",(long)totalComponents.day];
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
    CGFloat last;
    if (timeInterval < 0) {
        self.lastDays.text = @"0";
        last = 0;
    } else {
        NSDateComponents *lastComponents = [calendar components:unit fromDate:startDate toDate:currentDate options:0];
        self.lastDays.text = [NSString stringWithFormat:@"%ld",(long)lastComponents.day];
        last = lastComponents.day;
    }

    self.cropName.text = plant.productOptionName;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy/MM/dd";
    self.startTime.text = [formatter stringFromDate:startDate];
    self.endTime.text = [formatter stringFromDate:endDate];

    CGFloat progress = (CGRectGetWidth(self.bounds) - 2*16) * ( (totalComponents.day-last) / totalComponents.day);
    self.progressRightConstraint.constant = progress + 16;
    [self.contentView layoutIfNeeded];
}


@end
