//
//  CropGrowthCycleCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Plant;

@interface CropGrowthCycleCell : UITableViewCell

- (void)reloadWithPlant:(Plant *)plant;

@end
