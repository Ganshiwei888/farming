//
//  CropDetailCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/24.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "CropDetailCell.h"
#import "SubsidiaryCropDetail.h"

@interface CropDetailCell ()

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *farming;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *agricultrue;

@end

@implementation CropDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)reloadWithSubsidiaryCropDetail:(SubsidiaryCropDetail *)subsidiaryCropDetail {
    [self.icon sd_setImageWithURL:[NSURL URLWithString:subsidiaryCropDetail.image] placeholderImage:kPlaceHolderImage options:SDWebImageProgressiveDownload];
    self.farming.text = subsidiaryCropDetail.farmworkTypeName;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:ss";
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:subsidiaryCropDetail.operateTime];
    self.time.text = [dateFormatter stringFromDate:date];
    NSString *str = @"";
    NSInteger count = subsidiaryCropDetail.agriculturalProduct.count;
    for (NSInteger i = 0; i < count; i ++) {
        NSString *tempStr = @"";
        Agriculture *agriculture = subsidiaryCropDetail.agriculturalProduct[i];
        tempStr = [tempStr stringByAppendingFormat:@"%@%@%@",agriculture.agriculturalProductName,agriculture.agriculturalProductYield, agriculture.unit];
        if (i == 0) {
            str = [str stringByAppendingString:tempStr];
        } else {
            str = [str stringByAppendingFormat:@"·%@",tempStr];
        }

    }
    self.agricultrue.text = str;
}

@end
