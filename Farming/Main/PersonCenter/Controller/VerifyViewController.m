//
//  VerifyViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "VerifyViewController.h"
#import "ResetPasswordViewController.h"

@interface VerifyViewController ()
@property (weak, nonatomic) IBOutlet UILabel *tailNumber;
@property (weak, nonatomic) IBOutlet UITextField *verify;
@property (weak, nonatomic) IBOutlet UIButton *requestVerify;

@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"忘记密码";
    [self setupTailNumber];
}

- (void)setupTailNumber {
    NSString *tailNumberStr = [self.mobile substringFromIndex:self.mobile.length - 4];
    self.tailNumber.text = tailNumberStr;
}

- (IBAction)requestVerifyAction:(UIButton *)sender {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"mobile"] = self.mobile;
    parameters[@"type"] = @"repwd";
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kMobileVerify parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:baseResponse.msg];
        if (baseResponse.code == 0) {
            [self countdown];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"操作失败"];
    }];
}

- (IBAction)next:(id)sender {
    if (self.verify.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入验证码"];
        return;
    }
    ResetPasswordViewController *resetPasswordViewController = [[ResetPasswordViewController alloc] init];
    resetPasswordViewController.mobile = self.mobile;
    resetPasswordViewController.verifyCode = self.verify.text;
    [self.navigationController pushViewController:resetPasswordViewController animated:YES];
}

-(void)countdown{
    __block int timeout = 60; //倒计时时间

    __weak typeof(self ) weakSelf =self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.requestVerify setTitle:@"重发验证码" forState:UIControlStateNormal];
                weakSelf.requestVerify.enabled = YES;
            });
        }else{
            NSString *strTime = [NSString stringWithFormat:@"%.2d秒后重发", timeout];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.requestVerify.enabled = NO;
                weakSelf.requestVerify.titleLabel.text = strTime;
                [weakSelf.requestVerify setTitle:strTime forState:UIControlStateDisabled];
            });
            timeout--;

        }
    });
    dispatch_resume(timer);
}

@end
