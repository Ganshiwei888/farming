//
//  LoginViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "LoginViewController.h"
#import "MobileViewController.h"
#import <RESideMenu.h>
#import "CYLTabBarControllerConfig.h"
#import "PersonCenterViewController.h"
#import <UINavigationController+FDFullscreenPopGesture.h>
#import "User.h"
#import "DecodeResponseData.h"

static CGFloat const leftCosntraint = 28.0f;
static CGFloat const heightConstraint = 310.0f;
static CGFloat const topConstraint = 164.0f;

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopCosntraint;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self setupView];
}

- (void)setupView {
    self.viewWidthConstraint.constant = KScreenWidth - 2 * leftCosntraint;
    self.viewTopCosntraint.constant = kRealHeight(topConstraint);
    self.viewBottomConstraint.constant = MAX(KScreenHeight - kRealHeight(topConstraint) - heightConstraint, 80);
    [self.view layoutIfNeeded];
}
- (IBAction)forgetPassword:(id)sender {
    MobileViewController *mobileViewController = [[MobileViewController alloc] init];
    [self.navigationController pushViewController:mobileViewController animated:YES];
}

- (IBAction)commit:(id)sender {
    if (![self inputIsLegal]) {
        return;
    }
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    parameter[@"mobile"] = self.userName.text;
    parameter[@"pwd"] = self.password.text;
    [[RKNetworkHelper shareManager] POST:kUserLogin parameters:parameter success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            kAppWindow.rootViewController = [self sideMenu];
            [User initWithBaseRequest:baseResponse];
        } else {
            [MBProgressHUD showMessage:baseResponse.msg];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"操作失败"];
    }];
}

- (BOOL)inputIsLegal {
    if (self.userName.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入用户名"];
        return NO;
    }

    if (self.password.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入密码"];
        return NO;
    }
    return YES;
}

- (RESideMenu *)sideMenu {
    CYLTabBarControllerConfig *tabBarControllerConfig = [[CYLTabBarControllerConfig alloc] init];
    UITabBarController *tabBarController = tabBarControllerConfig.tabBarController;
    PersonCenterViewController *personCenterViewController = [[PersonCenterViewController alloc] init];
    RESideMenu *sideMenu = [[RESideMenu alloc] initWithContentViewController:tabBarController leftMenuViewController:personCenterViewController rightMenuViewController:nil];
    sideMenu.scaleContentView = NO;
    sideMenu.scaleMenuView = NO;
    sideMenu.scaleBackgroundImageView = NO;
    sideMenu.bouncesHorizontally = NO;
    sideMenu.fadeMenuView = NO;
    sideMenu.parallaxEnabled = NO;
    sideMenu.contentViewInPortraitOffsetCenterX = KScreenWidth/2.0 - 100;
    return sideMenu;
}

@end
