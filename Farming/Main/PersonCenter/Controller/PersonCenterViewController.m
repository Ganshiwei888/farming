//
//  PersonCenterViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/14.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "PersonCenterViewController.h"
#import "PersonCenterCell.h"
#import "ModifyPasswordViewController.h"
#import "BaseNavigationViewController.h"
#import <CYLTabBarController.h>
#import <RESideMenu.h>
#import "User.h"

static NSString *const personCenterCellIndentifier = @"PersonCenterCell";

@interface PersonCenterViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *userID;

@end

@implementation PersonCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

- (void)setupTableView {
    [self.tableView registerNib:[PersonCenterCell defaultNib] forCellReuseIdentifier:personCenterCellIndentifier];
    self.tableView.tableFooterView = [UIView new];
}

- (IBAction)logout:(id)sender {
    [User logout];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PersonCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:personCenterCellIndentifier forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RESideMenu *sideMenu = (RESideMenu *)kAppDelegate.window.rootViewController;
    CYLTabBarController *tabBarController = (CYLTabBarController *)sideMenu.contentViewController;
    BaseNavigationViewController *navigationViewController = (BaseNavigationViewController *)tabBarController.selectedViewController;
    ModifyPasswordViewController *modifyPasswordViewController = [[ModifyPasswordViewController alloc] init];
    [self.sideMenuViewController hideMenuViewController];
    [navigationViewController pushViewController:modifyPasswordViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
