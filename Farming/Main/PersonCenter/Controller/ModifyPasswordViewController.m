//
//  ModifyPasswordViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/15.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "ModifyPasswordViewController.h"

@interface ModifyPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;

@end

@implementation ModifyPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改密码";
}

- (IBAction)commit:(id)sender {
    if (![self inputIsLegal]) {
        return;
    }
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"oldPwd"] = self.oldPassword.text;
    parameters[@"newPwd"] = self.password.text;
    [[RKNetworkHelper shareManager] POST:kForgetPassword parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:baseResponse.msg];
        if (baseResponse.code == 0) {
            [self backAction];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"操作失败"];
    }];

}

- (BOOL)inputIsLegal {
    if (self.oldPassword.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入旧密码"];
        return NO;
    }

    if (self.password.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入新密码"];
        return NO;
    }

    if (self.confirmPassword.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入确认密码"];
        return NO;
    }

    if (![self.password.text isEqualToString:self.confirmPassword.text]) {
        [MBProgressHUD showMessage:@"新密码和确认密码不一致"];
        return NO;
    }
    return YES;
}


@end
