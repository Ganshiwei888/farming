//
//  MobileViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "MobileViewController.h"
#import "VerifyViewController.h"

@interface MobileViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mobile;

@end

@implementation MobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"忘记密码";
}

- (IBAction)next:(id)sender {
    if (![self inputIsLegal]) {
        return;
    }

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"mobile"] = self.mobile.text;
    parameters[@"type"] = @"repwd";
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kMobileVerify parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:baseResponse.msg];
        if (baseResponse.code == 0) {
            VerifyViewController *verifyViewController = [[VerifyViewController alloc] init];
            verifyViewController.mobile = self.mobile.text;
            [self.navigationController pushViewController:verifyViewController animated:YES];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"操作失败"];
    }];
}

- (BOOL)inputIsLegal {
    if (self.mobile.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入手机号"];
        return NO;
    }

    if (![Util checkTelNumber:self.mobile.text]) {
        [MBProgressHUD showMessage:@"请输入合法的手机号"];
        return NO;
    }
    return YES;
}


@end
