//
//  ResetPasswordViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/23.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"忘记密码";
}

- (IBAction)commit:(UIButton *)sender {
    if (![self inputIsLegal]) {
        return;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"mobile"] = self.mobile;
    parameters[@"verifyCode"] = self.verifyCode;
    parameters[@"pwd"] = self.password.text;
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    [[RKNetworkHelper shareManager] POST:kForgetPassword parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:baseResponse.msg];
        if (baseResponse.code == 0) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [MBProgressHUD showMessage:@"操作失败"];
    }];
}

- (BOOL)inputIsLegal {
    if (self.password.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入新密码"];
        return NO;
    }

    if (self.confirmPassword.text.length == 0) {
        [MBProgressHUD showMessage:@"请输入确认密码"];
        return NO;
    }

    if (![self.password.text isEqualToString:self.confirmPassword.text]) {
        [MBProgressHUD showMessage:@"两次密码输入不一致"];
        return NO;
    }
    return YES;
}

@end
