//
//  Farming.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/31.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "Farming.h"

@implementation Farming

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"plantArchives" : @"PlantArea"};
}

@end
