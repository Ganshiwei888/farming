//
//  FarmingPage.m
//  Farming
//
//  Created by 甘世伟 on 2017/9/25.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "FarmingPage.h"

@implementation FarmingPage

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"farmingList" : @"Farming"};
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"farmingList" : @"data"};
}

@end
