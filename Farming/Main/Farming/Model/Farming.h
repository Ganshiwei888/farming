//
//  Farming.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/31.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlantArea.h"

@interface Farming : NSObject

@property (nonatomic, copy) NSString * farmworkId;
@property (nonatomic, copy) NSString * typeName;
@property (nonatomic, copy) NSString * image;
@property (nonatomic, assign) NSTimeInterval operateTime;
@property (nonatomic, strong) NSArray<PlantArea *> * plantArchives;

@end
