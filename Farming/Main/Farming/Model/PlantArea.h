//
//  PlantArea.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/31.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlantArea : NSObject

@property (nonatomic, copy) NSString * plantArchivesId;
@property (nonatomic, copy) NSString * partitionName;
@property (nonatomic, copy) NSString * subareaName;
@property (nonatomic, copy) NSString * productId;
@property (nonatomic, copy) NSString * productOptionName;

@end
