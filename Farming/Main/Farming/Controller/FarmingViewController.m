 //
//  FarmingViewController.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/14.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "FarmingViewController.h"
#import <RESideMenu.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "FarmingViewCell.h"
#import "FarmingPage.h"
#import <MJRefresh.h>
#import "AddFarmingViewController.h"

static NSString *const farmingViewCellIdentifier = @"FarmingViewCell";

@interface FarmingViewController () <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray<Farming *> *dataArray;
@property (assign, nonatomic) NSInteger page;
@property (strong, nonatomic) FarmingPage *farmingPage;

@end

@implementation FarmingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    [self setupNavigation];
    [self setupTableView];
    [self setupDataWithPage:self.page];
    [self setupRefreshView];
}

- (void)setupNavigation {
    self.leftBarButtonItemImage = [UIImage imageNamed:@"user"];
}

- (void)setupTableView {
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 168;
    [self.tableView registerNib:[FarmingViewCell defaultNib] forCellReuseIdentifier:farmingViewCellIdentifier];
}

- (void)leftBarButtonItemAction {
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)setupDataWithPage:(NSInteger)page {
    [MBProgressHUD showHUDWithMessage:@"" toView:self.view];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"page"] = @(page);
    [[RKNetworkHelper shareManager] POST:kFarmingIndex parameters:parameters success:^(BaseResponse *baseResponse) {
        [MBProgressHUD hideHUDForView:self.view];
        if (baseResponse.code == 0) {
            self.farmingPage = [FarmingPage mj_objectWithKeyValues:baseResponse.data];
            if (page == 1) {
                self.dataArray = self.farmingPage.farmingList;
            } else {
                NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:self.dataArray];
                [tempArray addObjectsFromArray:self.farmingPage.farmingList];
                self.dataArray = tempArray;
            }

            [self.tableView reloadData];
        }
        [self endRefresh];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view];
        [self endRefresh];
    }];
}

- (void)setupRefreshView {
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self setupDataWithPage:self.page];
    }];

    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self setupDataWithPage:self.page];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];

    if (self.farmingPage.current_page < self.farmingPage.last_page) {
        [self.tableView.mj_footer endRefreshing];
    } else {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
    
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FarmingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:farmingViewCellIdentifier forIndexPath:indexPath];
    Farming *farming = self.dataArray[indexPath.row];
    [cell reloadCellWithIndexPath:indexPath farming:farming];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Farming *farming = self.dataArray[indexPath.row];
    AddFarmingViewController *addFarmingViewController = [[AddFarmingViewController alloc] init];
    addFarmingViewController.enterType = EnterViewControllerTypeFarmingDetail;
    addFarmingViewController.farmworkId = farming.farmworkId;
    [self.navigationController pushViewController:addFarmingViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"暂无农事" attributes:@{NSForegroundColorAttributeName: kFontGrayColor, NSFontAttributeName:kSystemFont(17.0)}];
    return str;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    self.page = 1;
    [self setupDataWithPage:self.page];
}

@end
