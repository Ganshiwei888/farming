//
//  FarmingViewCell.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/16.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "FarmingViewCell.h"
#import "Farming.h"

@interface FarmingViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *upLine;
@property (weak, nonatomic) IBOutlet UIImageView *circle;
@property (weak, nonatomic) IBOutlet UIImageView *dowmLine;
@property (weak, nonatomic) IBOutlet UILabel *farming;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *firstArea;
@property (weak, nonatomic) IBOutlet UIImageView *firstPosition;
@property (weak, nonatomic) IBOutlet UIImageView *secondPosition;
@property (weak, nonatomic) IBOutlet UILabel *secondArea;
@property (weak, nonatomic) IBOutlet UILabel *moreArea;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation FarmingViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)reloadCellWithIndexPath:(NSIndexPath *)indexPath farming:(Farming *)farming{
    if (indexPath.row == 0) {
        self.upLine.hidden = YES;
        self.circle.image = [UIImage imageNamed:@"solidCircle"];
    } else {
        self.upLine.hidden = NO;
        self.circle.image = [UIImage imageNamed:@"hollowCircle"];
    }

    if (farming.plantArchives.count == 0) {
        self.firstPosition.hidden = YES;
        self.firstArea.hidden = YES;
        self.secondPosition.hidden = YES;
        self.secondArea.hidden = YES;
        self.moreArea.hidden = YES;
    } else if (farming.plantArchives.count == 1 ) {
        self.secondPosition.hidden = YES;
        self.secondArea.hidden = YES;
        self.moreArea.hidden = YES;
        PlantArea *plantArea = farming.plantArchives[0];
        self.firstArea.text = [NSString stringWithFormat:@"%@\\%@\\%@", plantArea.partitionName, plantArea.subareaName, plantArea.productOptionName];
    } else if (farming.plantArchives.count >= 2) {
        if (farming.plantArchives.count == 2) {
            self.secondPosition.hidden = NO;
            self.secondArea.hidden = NO;
            self.moreArea.hidden = YES;
        } else {
            self.secondPosition.hidden = NO;
            self.secondArea.hidden = NO;
            self.moreArea.hidden = NO;
        }

        PlantArea *plantArea = farming.plantArchives[0];
        self.firstArea.text = [NSString stringWithFormat:@"%@\\%@\\%@", plantArea.partitionName, plantArea.subareaName, plantArea.productOptionName];
        PlantArea *plantArea1 = farming.plantArchives[1];
        self.secondArea.text = [NSString stringWithFormat:@"%@\\%@\\%@", plantArea1.partitionName, plantArea1.subareaName, plantArea1.productOptionName];
    }
    self.farming.text = farming.typeName;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:farming.operateTime];
    self.time.text = [self.dateFormatter stringFromDate:date];;
    [self.icon sd_setImageWithURL:[NSURL URLWithString:farming.image] placeholderImage:kPlaceHolderImage options:SDWebImageProgressiveDownload];
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    }
    return _dateFormatter;
}

@end
