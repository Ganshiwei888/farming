//
//  FarmingViewCell.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/16.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Farming;

@interface FarmingViewCell : UITableViewCell

- (void)reloadCellWithIndexPath:(NSIndexPath *)indexPath farming:(Farming *)farming;

@end
