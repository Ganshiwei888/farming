//
//  CYLTabBarControllerConfig.m
//  CYLTabBarController
//
//  v1.13.1 Created by 微博@iOS程序犭袁 ( http://weibo.com/luohanchenyilong/ ) on 10/20/15.
//  Copyright © 2015 https://github.com/ChenYilong . All rights reserved.
//
#import "CYLTabBarControllerConfig.h"
#import "FarmingViewController.h"
#import "PlantViewController.h"
#import "BaseNavigationViewController.h"

@interface CYLTabBarControllerConfig ()<UITabBarControllerDelegate>

@property (nonatomic, readwrite, strong) CYLTabBarController *tabBarController;

@end

@implementation CYLTabBarControllerConfig

- (CYLTabBarController *)tabBarController {
    if (_tabBarController == nil) {
        UIEdgeInsets imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        UIOffset titlePositionAdjustment = UIOffsetMake(0, MAXFLOAT);
        CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:self.viewControllers tabBarItemsAttributes:self.tabBarItemsAttributesForController imageInsets:imageInsets titlePositionAdjustment:titlePositionAdjustment];
        [self customizeTabBarAppearance:tabBarController];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)viewControllers {
    FarmingViewController *farmingViewController = [[FarmingViewController alloc] init];
    UIViewController *farmingNavigationController = [[BaseNavigationViewController alloc]
                                                   initWithRootViewController:farmingViewController];
    farmingViewController.navigationItem.title = @"农事";
    
    PlantViewController *plantViewController = [[PlantViewController alloc] init];
    UIViewController *plantNavigationController = [[BaseNavigationViewController alloc]
                                                    initWithRootViewController:plantViewController];
    plantViewController.navigationItem.title = @"种植";

  
    NSArray *viewControllers = @[
                                 farmingNavigationController,
                                 plantNavigationController,
                                 ];
    return viewControllers;
}

- (NSArray *)tabBarItemsAttributesForController {
    NSDictionary *firstTabBarItemsAttributes = @{
                                                 CYLTabBarItemImage : @"farmingUnselected",
                                                 CYLTabBarItemSelectedImage : @"farmingSelected",
                                                 };
    NSDictionary *secondTabBarItemsAttributes = @{
                                                  CYLTabBarItemImage : @"plantUnselected",
                                                  CYLTabBarItemSelectedImage : @"plantSelected",
                                                  };
    NSArray *tabBarItemsAttributes = @[
                                       firstTabBarItemsAttributes,
                                       secondTabBarItemsAttributes
                                       ];
    return tabBarItemsAttributes;
}


- (void)customizeTabBarAppearance:(CYLTabBarController *)tabBarController {
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
}

@end
