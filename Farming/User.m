//
//  User.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "User.h"
#import "BaseResponse.h"
#import "BaseNavigationViewController.h"
#import "LoginViewController.h"

@implementation User

+ (instancetype)initWithBaseRequest:(BaseResponse *)base{
    User *user = [User mj_objectWithKeyValues:base.data];
    NSMutableString *token = [base.responseData[@"token"] mutableCopy];
    user.token = [NSString stringWithString:[token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    user.userId = [base.responseData[@"userId"] integerValue];
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    if ([User allObjects].count == 0) {
        [defaultRealm transactionWithBlock:^{
            [defaultRealm addObject:user];
        }];
    }else{
        [defaultRealm transactionWithBlock:^{
            User *ouser = [User allObjects].firstObject;
            ouser.token = user.token;
            ouser.userId = user.userId;
            ouser.name = user.name;
            ouser.mobile = user.mobile;
        }];
    }

    return user;
}

+ (void)logout{
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    User *ouser = [User allObjects].firstObject;
    if (ouser) {
        [defaultRealm transactionWithBlock:^{
            ouser.token = @"";
            ouser.userId = 0;
            ouser.name = @"";
            ouser.mobile = @"";
        }];
    }
    [self setJPushTag];
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    BaseNavigationViewController *loginNavigationViewController = [[BaseNavigationViewController alloc] initWithRootViewController:loginViewController];
    kAppWindow.rootViewController = loginNavigationViewController;
}

+ (instancetype)getUser{
    User *user = [User allObjects].firstObject;
    return  user;
}

+ (void)setJPushTag{
//    [kAppDelegate performSelector:@selector(configJPushAlias)];
}

@end
