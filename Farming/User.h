//
//  User.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Realm/Realm.h>
@class BaseResponse;

@interface User : RLMObject

@property (copy, nonatomic) NSString *token;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *mobile;

+ (instancetype)initWithBaseRequest:(BaseResponse *)base;

+ (void)logout;

+ (instancetype)getUser;

+ (void)setJPushTag;

@end
