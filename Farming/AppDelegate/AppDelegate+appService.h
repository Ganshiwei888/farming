//
//  AppDelegate+appService.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/15.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (appService)

- (void)initWindow;

@end
