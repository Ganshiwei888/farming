//
//  AppDelegate.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/10.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+appService.h"
#import <AFNetworking.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [self initWindow];
    [[RKNetworkHelper shareManager] appInitWithSuccess:^(BaseResponse *baseResponse) {

    } failure:^(NSError *error) {

    }];
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(UIApplicationExtensionPointIdentifier)extensionPointIdentifier {
    return NO;
}


@end
