//
//  AppDelegate+appService.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/15.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AppDelegate+appService.h"
#import "CYLPlusButtonSubclass.h"
#import "CYLTabBarControllerConfig.h"
#import "PersonCenterViewController.h"
#import "LoginViewController.h"
#import <RESideMenu.h>
#import "User.h"
#import "BaseNavigationViewController.h"
#import <Bugly/Bugly.h>
#import "ThirdMacros.h"

@implementation AppDelegate (appService) 

- (void)initWindow {
    [self showDebugging];
    [self handleRealm];
    [CYLPlusButtonSubclass registerPlusButton];
    [Bugly startWithAppId:kBuglyAPPID];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = kBackgroundColor;
    User *user = [User getUser];
    if (user && user.userId != 0) {
        self.window.rootViewController = [self sideMenu];
    } else {
        self.window.rootViewController = [self loginViewController];
    }
    [self.window makeKeyAndVisible];
    
}

- (RESideMenu *)sideMenu {
    CYLTabBarControllerConfig *tabBarControllerConfig = [[CYLTabBarControllerConfig alloc] init];
    UITabBarController *tabBarController = tabBarControllerConfig.tabBarController;
    PersonCenterViewController *personCenterViewController = [[PersonCenterViewController alloc] init];
    RESideMenu *sideMenu = [[RESideMenu alloc] initWithContentViewController:tabBarController leftMenuViewController:personCenterViewController rightMenuViewController:nil];
    sideMenu.scaleContentView = NO;
    sideMenu.scaleMenuView = NO;
    sideMenu.scaleBackgroundImageView = NO;
    sideMenu.bouncesHorizontally = NO;
    sideMenu.fadeMenuView = NO;
    sideMenu.parallaxEnabled = NO;
    sideMenu.contentViewInPortraitOffsetCenterX = KScreenWidth/2.0 - 100;
    return sideMenu;
}

- (UIViewController *)loginViewController {
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    BaseNavigationViewController *loginNavigationViewController = [[BaseNavigationViewController alloc] initWithRootViewController:loginViewController];
    return loginNavigationViewController;
}

- (void)showDebugging{
#ifdef DEBUG
Class class = NSClassFromString(@"UIDebuggingInformationOverlay");
[class performSelector:@selector(prepareDebuggingOverlay)];
[class performSelector:@selector(overlay)];
#endif

}

- (void)handleRealm{
    NSString *newVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    NSMutableArray<NSString *> *temp = [[newVersion componentsSeparatedByString:@"."] mutableCopy];
    if (temp.count < 3) {
        if (temp.count == 1) {
            [temp addObjectsFromArray:@[@"0",@"0"]];
        }else if (temp.count == 2){
            [temp addObject:@"0"];
        }
    }
    __block NSString *str = @"";
    [temp enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        str =  [str stringByAppendingString:obj];
    }];
    //schemaVersion 一定不能比oldschemaVersion小,不然会崩溃；比oldschemaVersion大，realm\才会对数据库的结构进行检测升级，建议版本按1.0.0这种格式写最多三位

    config.schemaVersion = str.integerValue;

    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldschemaVersion){
        if (oldschemaVersion < str.integerValue) {

        }
    };
    [RLMRealmConfiguration setDefaultConfiguration:config];
    [RLMRealm defaultRealm];
}


@end
