//
//  UIImageView+addDelete.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/17.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DeleteBlock)(UIImage *image);

@interface UIImageView (addDelete)

@property (nonatomic, strong) DeleteBlock deleteBlock; 

- (void)addDeleteFunction;

@end
