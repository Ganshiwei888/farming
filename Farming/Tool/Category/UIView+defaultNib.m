//
//  UIView+defaultNib.m
//  LeXianStaff
//
//  Created by ganshiwei on 17/3/16.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "UIView+defaultNib.h"

@implementation UIView (defaultNib)

+(UINib *)defaultNib{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

@end
