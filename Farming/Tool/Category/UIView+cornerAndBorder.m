//
//  UIView+cornerAndBorder.m
//  YiDaoHuo
//
//  Created by BOX on 16/9/13.
//  Copyright © 2016年 ZhaiJia. All rights reserved.
//

#import "UIView+cornerAndBorder.h"

@interface UIView ()

@end

@implementation UIView (cornerAndBorder)

-(void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
}

- (CGFloat)cornerRadius{
    return self.layer.cornerRadius;
}

- (void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = borderColor.CGColor;
}

- (UIColor *)borderColor{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth{
    self.layer.borderWidth = borderWidth;
}

- (CGFloat)borderWidth{
    return self.layer.borderWidth;
}



@end
