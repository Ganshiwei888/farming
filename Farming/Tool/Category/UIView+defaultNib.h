//
//  UIView+defaultNib.h
//  LeXianStaff
//
//  Created by ganshiwei on 17/3/16.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (defaultNib)

+ (UINib *)defaultNib;

@end
