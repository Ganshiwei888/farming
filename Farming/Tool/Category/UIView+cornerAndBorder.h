//
//  UIView+cornerAndBorder.h
//  YiDaoHuo
//
//  Created by BOX on 16/9/13.
//  Copyright © 2016年 ZhaiJia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (cornerAndBorder)

///圆角
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
///边框宽
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
///边框颜色
@property (nonatomic, strong) IBInspectable UIColor *borderColor;

@end
