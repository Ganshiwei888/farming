//
//  UIImageView+addDelete.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/17.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "UIImageView+addDelete.h"
#import <Masonry.h>

static char *key = "deleteBlock";

@implementation UIImageView (addDelete)

- (void)addDeleteFunction {
    self.userInteractionEnabled = YES;
    UIButton *button= [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"deleteImage"] forState:UIControlStateNormal];
    [button  addTarget:self action:@selector(deleteSelf) forControlEvents:UIControlEventTouchUpInside];

    [self addSubview:button ];

    [button  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(@20);
        make.top.right.equalTo(self);
    }];

}

- (void)deleteSelf {
    if (self.deleteBlock) {
        self.deleteBlock(self.image);
    }
}

- (void)setDeleteBlock:(DeleteBlock)deleteBlock {
    objc_setAssociatedObject(self, key, deleteBlock, OBJC_ASSOCIATION_RETAIN);
}

- (DeleteBlock)deleteBlock {
    return objc_getAssociatedObject(self, key);
}

@end
