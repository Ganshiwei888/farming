//
//  Util.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CommonOperationContent;

@interface Util : NSObject

+ (UIImage *)drawLinearGradientWith:(CGRect)p_rect;
+ (void)handleCommonOperation:(CommonOperationContent *)commonOperation;
+ (NSString *)md5:(NSString *)str;
+ (NSString*)getAppVersion;
///检测手机号是否合法
+ (BOOL)checkTelNumber:(NSString *)telNumber;
///检测密码格式是否正确
+ (BOOL)checkPassword:(NSString *)password;
///检测姓名格式是否正确
+ (BOOL)checkUserName:(NSString *)userName;
///检测身份证号是否正确
+ (BOOL)checkUserIdCard:(NSString *)idCard;
///检测验证码是否是6位数字
+ (BOOL)checkVerifyCode:(NSString *)verifyCode;
///检测银行卡是否合法
+ (BOOL)checkCardNo:(NSString*)cardNo;

@end
