//
//  Util.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "Util.h"
#import <CommonCrypto/CommonDigest.h>
#import "CommonOperationContent.h"

@implementation Util

+ (UIImage *)drawLinearGradientWith:(CGRect)p_rect {
    CGRect rect = p_rect;
    rect.size.height += 20;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:rect];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = {0.0, 1.0};
    NSArray *colors = @[(__bridge id)(kNavigationLeftColor.CGColor), (__bridge id)(kNavigationRightColor.CGColor)];
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors, locations);
    CGRect pathRect = CGPathGetBoundingBox(path.CGPath);

    CGPoint startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMinY(pathRect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMinY(pathRect));

    CGContextSaveGState(context);
    CGContextAddPath(context, path.CGPath);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
    return image;
}


+ (void)handleCommonOperation:(CommonOperationContent *)commonOperation {
    NSString *str = commonOperation.farmworkTypeId;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"farmworkTypeId = %@", str];
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    RLMResults <CommonOperationContent *> *operationContents = [CommonOperationContent objectsWithPredicate:pred];
    RLMResults <CommonOperationContent *> *allOperationContents = [CommonOperationContent allObjects];
    NSInteger count = operationContents.count;
    NSInteger allCount = allOperationContents.count;
    if (count > 0) {
        [defaultRealm transactionWithBlock:^{
            [defaultRealm deleteObjects:operationContents];
            [defaultRealm addObject:commonOperation];
        }];
    } else {
        if (allCount == 10) {
            [defaultRealm transactionWithBlock:^{
                [defaultRealm deleteObject:allOperationContents.firstObject];
                [defaultRealm addObject:commonOperation];
            }];
        } else {
            [defaultRealm transactionWithBlock:^{
                [defaultRealm addObject:commonOperation];
            }];
        }
    }
}

+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (NSString*)getAppVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleShortVersionString"];
}

#pragma mark - 正则匹配手机号
+ (BOOL)checkTelNumber:(NSString *)telNumber
{
    NSString *pattern = @"^1+[3578]+\\d{9}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:telNumber];
    return isMatch;
}


#pragma mark - 正则匹配用户密码6-14位数字、字母、#*._@组合
+ (BOOL)checkPassword:(NSString *)password
{
    NSString *pattern = @"^[a-zA-Z0-9#*._@]{6,14}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;

}

#pragma mark - 正则匹配用户姓名,20位的中文、英文
+ (BOOL)checkUserName:(NSString *)userName
{
    NSString *pattern = @"^[a-zA-Z\u4E00-\u9FA5]{1,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:userName];
    return isMatch;

}

#pragma mark - 正则匹配用户身份证号15或18位
+ (BOOL)checkUserIdCard:(NSString *)idCard
{
    NSString *pattern = @"(^[0-9]{15}$)|([0-9]{17}([0-9]|X)$)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:idCard];
    return isMatch;
}

#pragma mark - 验证码6位
+ (BOOL)checkVerifyCode:(NSString *)verifyCode{
    NSString *pattern = @"^[0-9]{6}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:verifyCode];
    return isMatch;
}

#pragma mark - 银行卡
+ (BOOL)checkCardNo:(NSString*) cardNo{
    int oddsum = 0;     //奇数求和
    int evensum = 0;    //偶数求和
    int allsum = 0;
    int cardNoLength = (int)[cardNo length];
    int lastNum = [[cardNo substringFromIndex:cardNoLength-1] intValue];

    cardNo = [cardNo substringToIndex:cardNoLength - 1];
    for (int i = cardNoLength -1 ; i>=1;i--) {
        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(i-1, 1)];
        int tmpVal = [tmpString intValue];
        if (cardNoLength % 2 ==1 ) {
            if((i % 2) == 0){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }else{
            if((i % 2) == 1){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }
    }

    allsum = oddsum + evensum;
    allsum += lastNum;
    if((allsum % 10) == 0)
        return YES;
    else
        return NO;
}

@end
