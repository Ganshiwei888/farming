//
//  GolfButlerTakePhotoUtility.h
//  GolfButler
//
//  Created by Aorunde on 15/6/24.
//  Copyright (c) 2015年 Aorunde. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImagePickerToolDelegate <NSObject>

-(void)TakePhotoToProcess:(UIImage *)xImage;

@end

@interface ImagePickerTool : NSObject<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImagePickerController *pickerController;
}

/**
 *  @author taohebin, 15-06-24 16:06:01
 *
 *  暂时只支持viewcontroler类型
 */
@property (nonatomic ,   weak) UIViewController<ImagePickerToolDelegate> *delegate;



/**
 *  @author taohebin, 15-06-24 17:06:29
 *
 *  单例得到唯一实例
 *
 *  @return 返回实例
 */
+(ImagePickerTool *)shareTool;

/**
 *  @author taohebin, 15-06-24 16:06:21
 *
 *  开始拍照片
 */
-(void)startTakePhotowithAllowsEditing:(BOOL)allowsEditing;

/**
 *  @author taohebin, 15-06-24 16:06:48
 *
 *  相册获取照片
 */
-(void)startPhotoFromAlbumwithAllowsEditing:(BOOL)allowsEditing;


/**
 *  @author taohebin, 15-06-25 11:06:29
 *
 *  保存图片到相册
 */
-(void)saveImageToPhotos:(UIImage*)savedImage;

@end
