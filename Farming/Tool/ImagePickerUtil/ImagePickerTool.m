//
//  GolfButlerTakePhotoUtility.m
//  GolfButler
//
//  Created by Aorunde on 15/6/24.
//  Copyright (c) 2015年 Aorunde. All rights reserved.
//

#import "ImagePickerTool.h"

@implementation ImagePickerTool

+(ImagePickerTool *)shareTool{
    static ImagePickerTool *util = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        util = [[ImagePickerTool alloc]init];
    });
    
    return util;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        pickerController = [[UIImagePickerController alloc] init];
    }
    return self;
}

-(void)startTakePhotowithAllowsEditing:(BOOL)allowsEditing{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        pickerController.mediaTypes =[NSArray arrayWithObjects:@"public.image", nil];
        pickerController.delegate = self;
        pickerController.allowsEditing = allowsEditing;
    }
    [_delegate presentViewController:pickerController animated:YES completion:nil];
}

-(void)startPhotoFromAlbumwithAllowsEditing:(BOOL)allowsEditing{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSArray *tempTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerController.sourceType];
        pickerController.mediaTypes = tempTypes;
        pickerController.delegate = self;
        pickerController.allowsEditing = allowsEditing;
    }
    [_delegate presentViewController:pickerController animated:YES completion:nil];
}


#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *mediaType = info[UIImagePickerControllerMediaType];

        [picker dismissViewControllerAnimated:YES completion:nil];
        
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage *image = info[@"UIImagePickerControllerEditedImage"];
        UIImage *OriginalImage = info[@"UIImagePickerControllerOriginalImage"];
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            [self saveImageToPhotos:OriginalImage];
        }
        if (image) {
            [_delegate TakePhotoToProcess:image];
        }else{
            [_delegate TakePhotoToProcess:OriginalImage];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)saveImageToPhotos:(UIImage*)savedImage

{

    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);

}

// 指定回调方法

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    DLog(@"%@", msg);
}





@end
