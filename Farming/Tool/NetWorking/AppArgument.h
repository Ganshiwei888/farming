//
//  AppArgument.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Realm/Realm.h>

@interface AppArgument : RLMObject

@property (copy, nonatomic) NSString *token;
@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *fileUploadType;
@property (copy, nonatomic) NSString *host;
@property (copy, nonatomic) NSString *accessId;
@property (copy, nonatomic) NSString *accessKey;
@property (copy, nonatomic) NSString *bucket;
@property (copy, nonatomic) NSString *serviceTel;
@property (copy, nonatomic) NSString *appDownUrl;
@property (copy, nonatomic) NSString *appVersion;
@property (copy, nonatomic) NSString *appVersionCode;
@property (copy, nonatomic) NSString *upgradeInfo;
@property (copy, nonatomic) NSString *forceUpgrade;

@property (assign, nonatomic) int ivint;

+ (instancetype)getAppArgument;

@end
