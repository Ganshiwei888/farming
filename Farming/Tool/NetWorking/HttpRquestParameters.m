//
//  HttpRquestParameters.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "HttpRquestParameters.h"
#import "AppArgument.h"
#import "User.h"

@implementation HttpRquestParameters

#pragma mark - PrivateMethod

+ (NSDictionary *)addUniformParameters:(NSDictionary *)parameters isNeedToken:(BOOL)isNeedToken {
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:parameters];

    dic[kDeviceType] = DeviceType;

    NSUUID *uuid = [UIDevice currentDevice].identifierForVendor;
    NSString *uuidString = uuid.UUIDString;
    dic[kDeviceID] = uuidString;

    NSString *version = [Util getAppVersion];
    dic[kAppVersion] = version;

    NSString *deviceModel = [[UIDevice currentDevice] machineModelName];
    dic[kBrandName] = deviceModel;

    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    dic[kOsVersion] = systemVersion;

    NSString *token = [self getToken];
    NSString *userId = [self getUserId];
    if (isNeedToken) {
        dic[kToken] = token;
        dic[kUserId] = userId;
    }

    return [dic copy];
}

+ (NSString *)getToken {
    NSString *token = [self getUserToken];
    if (kStringIsEmpty(token) ) {
        AppArgument *appArgument = [AppArgument getAppArgument];
        return appArgument.token;
    }

    return token;
}

+ (NSString*)getUserId {
    User *user = [User getUser];
    return [NSString stringWithFormat:@"%ld",(long)user.userId];
}

+ (NSString *)getUserToken {
    User *user = [User getUser];
    return user.token;
}

@end
