//
//  AppArgument.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "AppArgument.h"

@implementation AppArgument

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"host":@"data.fileUploadConfig.host",
             @"accessId":@"data.fileUploadConfig.accessId",
             @"accessKey":@"data.fileUploadConfig.accessKey",
             @"bucket":@"data.fileUploadConfig.bucket",
             @"fileUploadType":@"data.fileUploadType",
             @"serviceTel":@"data.serviceTel",
             @"appDownUrl":@"data.appDownUrl",
             @"appVersion":@"data.appVersion",
             @"appVersionCode":@"data.appVersionCode",
             @"upgradeInfo":@"data.upgradeInfo",
             @"forceUpgrade":@"data.forceUpgrade"};
}

+ (NSArray *)mj_ignoredPropertyNames {
    return @[@"ivint"];
}

+ (instancetype)getAppArgument {
    return [AppArgument allObjects].firstObject;
}

@end
