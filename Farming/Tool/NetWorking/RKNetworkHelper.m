//
//  RKNetworkHelper.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "RKNetworkHelper.h"
#import <AFNetworking.h>
#import "BaseResponse.h"
#import "HttpRquestParameters.h"
#import <AFNetworkActivityIndicatorManager.h>
#import <YYKit/UIApplication+YYAdd.h>
#import "DecodeResponseData.h"
#import "User.h"
#import "AppArgument.h"
#import <Realm.h>
#import <Reachability.h>

@interface RKNetworkHelper ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManger;
@property (nonatomic, strong) NSMutableArray <NSURLSessionDataTask *> *allSessionTask;
@property (nonatomic, strong) Reachability *reachability;

@end

@implementation RKNetworkHelper

static RKNetworkHelper *_networkHelper = nil;

+ (instancetype)shareManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _networkHelper = [[RKNetworkHelper alloc] init];
    });
    return _networkHelper;
}

- (instancetype)init {
    if (self = [super init]) {
        AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kHOST]];
        sessionManager.requestSerializer.timeoutInterval = 30.0f;
        sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*",@"text/encode", nil];
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
        self.sessionManger = sessionManager;
        self.reachability = [Reachability reachabilityForInternetConnection];
        [self.reachability startNotifier];
    }
    return self;
}

- (BOOL)isNetwork {
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showMessage:@"无法连接到网络,请稍后重试"];
        });
    }
    return remoteHostStatus != NotReachable;
}

- (void)cancelRequestWithURL:(NSString *)url {
    if (!url) {
        return;
    }
    url = [NSString stringWithFormat:@"%@%@", kHOST, url];

    @synchronized (self) {
        [self.allSessionTask enumerateObjectsUsingBlock:^(NSURLSessionDataTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.currentRequest.URL.absoluteString hasPrefix:url]) {
                [obj cancel];
                [self.allSessionTask removeObject:obj];
                *stop = YES;
            }
        }];
    }
}

- (void)cancelAllRequest {
    @synchronized (self) {
        [self.allSessionTask enumerateObjectsUsingBlock:^(NSURLSessionDataTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj cancel];
        }];
        [self.allSessionTask removeAllObjects];
    }
}

#pragma mark - UploadImages

- (NSURLSessionDataTask *)uploadImagesWithUrl:(NSString *)url parameters:(NSDictionary *)parameters name:(NSString *)name images:(NSArray<UIImage *> *)images fileNames:(NSArray<NSString *> *)fileNames imageScale:(CGFloat)imageScale imageType:(NSString *)imageType success:(RKHttpRequestSuccess)success failure:(RKHttpRequestFailed)failure {
    if (![self isNetwork]) {
        NSError *error = [[NSError alloc] initWithDomain:@"com.noNetwork.error" code:0 userInfo:nil];
        failure(error);
        return nil;
    }

    NSDictionary *dic = [[NSDictionary alloc] init];

    if ([url isEqualToString:kAppInit] || [url isEqualToString:kUserLogin]) {
        dic = [HttpRquestParameters addUniformParameters:parameters isNeedToken:NO];
    } else {
        dic = [HttpRquestParameters addUniformParameters:parameters isNeedToken:YES];
    }

    DLog(@"url:%@, parameters:%@, addUniformParamters:%@",url,parameters,dic);

    NSURLSessionDataTask *dataTask = [self.sessionManger POST:url parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSInteger count = images.count;
        for (NSInteger i = 0; i < count ; i ++) {
            NSData *imageData = UIImageJPEGRepresentation(images[i], imageScale ? : 1.f);
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName;
            if (fileNames.count > 0) {
                fileName  = [NSString stringWithFormat:@"%@.%@",fileNames[i], imageType ? : @"jpg"];
            } else {
                fileName =  [NSString stringWithFormat:@"%@%ld.%@",str,i,imageType ? : @"jpg"];
            }
            NSString *mimeType = [NSString stringWithFormat:@"image/%@", imageType ? : @"jpg"];
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:mimeType];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"%@", task.currentRequest.allHTTPHeaderFields);
        [self handleResponseObject:responseObject url:url success:success dataTask:task];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"网络请求错误信息：error: %@,url:%@,parameters:%@", error.description,url, parameters);
        [self.allSessionTask removeObject:task];
        failure ? failure(error) : nil;
        if ([error.description containsString:@"timed out"] || [error.description containsString:@"offline"]) {
            [MBProgressHUD showMessage:@"网络请求超时"];
        }
    }];

    dataTask ? [self.allSessionTask addObject:dataTask] : nil;
    return dataTask;
}

#pragma mark - Post

- (NSURLSessionDataTask *)POST:(NSString *)url parameters:(NSDictionary *)parameters success:(RKHttpRequestSuccess)success failure:(RKHttpRequestFailed)failure {
    if (![self isNetwork]) {
        NSError *error = [[NSError alloc] initWithDomain:@"com.noNetwork.error" code:0 userInfo:nil];
        failure(error);
        return nil;
    }
    NSDictionary *dic = [[NSDictionary alloc] init];

    if ([url isEqualToString:kAppInit] || [url isEqualToString:kUserLogin]) {
        dic = [HttpRquestParameters addUniformParameters:parameters isNeedToken:NO];
    } else {
        dic = [HttpRquestParameters addUniformParameters:parameters isNeedToken:YES];
    }

    DLog(@"url:%@, parameters:%@, addUniformParamters:%@",url,parameters,dic);

    AppArgument *appArgument = [AppArgument getAppArgument];
    BOOL appArgumentNotExist = !appArgument && ![url isEqualToString:kAppInit];
    __block NSURLSessionDataTask *dataTask;
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    if (appArgumentNotExist) {
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            [self appInitWithSuccess:^(BaseResponse *baseResponse) {
                dispatch_group_leave(group);
            } failure:^(NSError *error) {
                dispatch_group_leave(group);
            }];
        });
        // 执行此方法获取到的dataTask为空需要使用代理传出dataTask
        dispatch_group_notify(group, queue, ^{
            dataTask = [self useAFNetWorkingWithUrl:url parameters:dic success:success failure:failure];
            dataTask ? [self.allSessionTask addObject:dataTask] : nil;
        });
    } else {
        dataTask = [self useAFNetWorkingWithUrl:url parameters:dic success:success failure:failure];
    }

    dataTask ? [self.allSessionTask addObject:dataTask] : nil;

    return dataTask;
}

#pragma mark - appInit

- (NSURLSessionDataTask *)appInitWithSuccess:(RKHttpRequestSuccess)success failure:(RKHttpRequestFailed)failured {
    NSURLSessionDataTask *dataTask = [self POST:kAppInit parameters:nil success:^(BaseResponse *baseResponse) {
        if (baseResponse.code == 0) {
            AppArgument *appArgument = [AppArgument mj_objectWithKeyValues:baseResponse.responseData];
            RLMRealm *defaultRealm = [RLMRealm defaultRealm];
            if ([AppArgument allObjects].count == 0) {
                [defaultRealm transactionWithBlock:^{
                    [defaultRealm addObject:appArgument];
                }];
            } else {
                [defaultRealm transactionWithBlock:^{
                    AppArgument *rAppArgurment = [AppArgument allObjects].firstObject;
                    rAppArgurment = appArgument;
                }];
            }
        }
        success ? success(baseResponse) : nil;
    } failure:^(NSError *error) {
        failured ? failured(error) : nil;
    }];

    return dataTask;
}

#pragma mark - Private

- (NSURLSessionDataTask *)useAFNetWorkingWithUrl:(NSString *)url parameters:(NSDictionary *)parameters success:(RKHttpRequestSuccess)success failure:(RKHttpRequestFailed)failure{
    NSURLSessionDataTask *dataTask = [self.sessionManger POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self handleResponseObject:responseObject url:url success:success dataTask:task];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"网络请求错误信息：error: %@,url:%@,parameters:%@", error.description,url, parameters);
        [self.allSessionTask removeObject:task];
        failure ? failure(error) : nil;
        if ([error.description containsString:@"timed out"] || [error.description containsString:@"offline"]) {
            [MBProgressHUD showMessage:@"网络请求超时"];
        }
    }];
    return dataTask;
}

#pragma mark - 处理数据
- (void)handleResponseObject:(id)responseObject url:(NSString *)url success:(RKHttpRequestSuccess)success dataTask:(NSURLSessionDataTask *)task{
    [self.allSessionTask removeObject:task];
    NSDictionary *decodeData;
    if ([url isEqualToString:kAppInit]) {
        decodeData = [DecodeResponseData decodeResponseData:(NSDictionary *)responseObject isInit:YES];
    } else {
        decodeData = [DecodeResponseData decodeResponseData:(NSDictionary *)responseObject isInit:NO];
    }
    BaseResponse *baseResponse = [[BaseResponse alloc] initWithDic:decodeData];
    NSInteger code = baseResponse.code;
    success ? success(baseResponse) : nil;
    if (code != 0) {
        [self checkTokenIsNormal:code];
#ifdef DEBUG
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showMessage:baseResponse.msg];
        });
#endif
    }
}

#pragma mark - 检查token是否过期

- (void)checkTokenIsNormal:(NSInteger)code {
    if (code == 99997) {
        [User logout];
    }
}

#pragma mark - Getter

- (NSMutableArray *)allSessionTask {
    if (!_allSessionTask) {
        _allSessionTask = [[NSMutableArray alloc] init];
    }
    return _allSessionTask;
}

@end
