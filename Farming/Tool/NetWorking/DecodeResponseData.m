//
//  DecodeResponseData.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "DecodeResponseData.h"
#import "User.h"
#import "AppArgument.h"
#import "CBCUtil.h"
#import "BaseResponse.h"

@implementation DecodeResponseData

+ (NSDictionary *)decodeResponseData:(NSDictionary *)responseData isInit:(BOOL)isInit {
    BaseResponse *baseResponse = [[BaseResponse alloc] initWithDic:responseData];
    id data = baseResponse.data;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:responseData];
    if (responseData && data && [data isKindOfClass:[NSString class]]) {
        int ivint = 0;
        NSString *key = @"";
        if (isInit) {
            ivint = [self getIndexWithKey:baseResponse.key];
            NSString *userId = @"0";
            key = baseResponse.token;
            key = [Util md5:[key stringByAppendingString:userId]];
        } else {
            ivint = [self getIndexWithKey:[AppArgument getAppArgument].key];
            key = baseResponse.token;
            if (kStringIsEmpty(key)) {
                key = [self getToken];
            }
            NSString *userId = [NSString stringWithFormat:@"%d",baseResponse.userId];
            if ([userId isEqualToString:@"0"]) {
                userId = [self getUserId];
            }
            key = [Util md5:[key stringByAppendingString:userId]];
        }
        ivint = [self gettopestV:ivint];
        NSString *decodeData = [CBCUtil CBCDecrypt:data key:key index:ivint];
        NSError *error = nil;
        id dataObject = nil;
        if (decodeData) {
            dataObject = [NSJSONSerialization JSONObjectWithData:[decodeData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
        }

        if (dataObject || baseResponse.code == 0) {
            if (kObjectIsEmpty(dataObject)) {
                dic[@"data"] = @"";
            } else {
                dic[@"data"] = dataObject;
            }
        }
    }
    DLog(@"解析后数据：%@", dic);
    return [dic copy];
}

+ (int)getIndexWithKey:(NSString *)key {
    char keyPtr[10]={0};
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    return (int)strtoul(keyPtr,NULL,24);
}

+ (int)gettopestV:(int)v {
    int r = v;
    while ( r > 10 ) {
        r  = r/10;
    }
    return r;
}

+ (NSString *)getToken {
    NSString *token = [self getUserToken];
    if (kStringIsEmpty(token) ) {
        AppArgument *appArgument = [AppArgument getAppArgument];
        return appArgument.token;
    }
    return token;
}

+ (NSString*)getUserId {
    User *user = [User getUser];
    return [NSString stringWithFormat:@"%ld",(long)user.userId];
}

+ (NSString *)getUserToken {
    User *user = [User getUser];
    return user.token;
}

@end
