//
//  BaseResponse.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "BaseResponse.h"

@implementation BaseResponse

- (BaseResponse *)initWithDic:(NSDictionary *)dic {
    BaseResponse *baseResponse = [BaseResponse mj_objectWithKeyValues:dic];
    baseResponse.responseData = dic;
    return  baseResponse;
}

@end
