//
//  RKNetworkHelper.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseResponse;

typedef void(^RKHttpRequestSuccess) (BaseResponse *baseResponse);
typedef void(^RKHttpRequestFailed) (NSError *error);

@interface RKNetworkHelper : NSObject

+ (instancetype)shareManager;

- (BOOL)isNetwork;

- (void)cancelRequestWithURL:(NSString *)url;

- (void)cancelAllRequest;

- (__kindof NSURLSessionDataTask *)POST:(NSString *)url
                         parameters:(NSDictionary *)parameters
                            success:(RKHttpRequestSuccess)success
                            failure:(RKHttpRequestFailed)failure;

- (__kindof NSURLSessionDataTask *)uploadImagesWithUrl:(NSString *)url
                                            parameters:(NSDictionary *)parameters
                                                  name:(NSString *)name
                                                images:(NSArray<UIImage *> *)images
                                             fileNames:(NSArray<NSString *> *)fileNames
                                            imageScale:(CGFloat)imageScale
                                             imageType:(NSString *)imageType
                                               success:(RKHttpRequestSuccess)success
                                               failure:(RKHttpRequestFailed)failure;

- (__kindof NSURLSessionDataTask *)appInitWithSuccess:(RKHttpRequestSuccess)success
                                              failure:(RKHttpRequestFailed)failured;

@end
