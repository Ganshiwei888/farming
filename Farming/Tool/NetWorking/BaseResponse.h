//
//  BaseResponse.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseResponse : NSObject

@property (nonatomic, assign) int code;
@property (nonatomic, assign) int userId;
@property (nonatomic, strong) id data;
@property (nonatomic, copy) NSString *debug;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *msg;

//在app.init里使用此字段，一般解析使用data字段
@property (nonatomic, copy) NSDictionary *responseData;

-(BaseResponse *)initWithDic:(NSDictionary*)dic;

@end
