//
//  DecodeResponseData.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DecodeResponseData : NSObject

+ (NSDictionary *)decodeResponseData:(id)responseData isInit:(BOOL)isInit;

@end
