//
//  ExternString.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kDeleteArea;
extern NSString *const kSearchOperationContent;
extern NSString *const kSearchAgriculture;
extern NSString *const kSearchCrop;

typedef NS_ENUM(NSInteger, EnterViewControllerType) {
    EnterViewControllerTypeAddFarming,
    EnterViewControllerTypeFarmingDetail,
    EnterViewControllerTypeCropDetail
};

@interface ExternString : NSObject

@end
