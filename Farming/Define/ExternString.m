//
//  ExternString.m
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#import "ExternString.h"

NSString * const kDeleteArea = @"kDeleteArea";
NSString * const kSearchOperationContent = @"kSearchOperationContent";
NSString * const kSearchAgriculture = @"kSearchAgriculture";
NSString * const kSearchCrop = @"kSearchCrop";

@implementation ExternString

@end
