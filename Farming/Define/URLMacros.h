//
//  URLMacros.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#ifndef URLMacros_h
#define URLMacros_h

#ifdef DEBUG
#define kHOST   @"http://api.trace.zhaioto.com/seller/"
#else
#define kHOST   @"http://api.trace.zhaioto.com/seller/"
#endif

#define DeviceType    @"ios"
#define kDeviceType   @"deviceType"
#define kDeviceID     @"deviceId"
#define kAppVersion   @"appVersion"
#define kUserId       @"userId"
#define kBrandName    @"brandName"
#define kOsVersion    @"osVersion"
#define kToken        @"token"

#define kAppInit           @"app.init"
#define kUserLogin         @"user.login"
#define kMobileVerify      @"User.mobileverify"
#define kForgetPassword    @"User.forgetpasswords"
#define kResetPassword     @"User.renewpwd"
#define kFarmingIndex      @"farmwork.index"
#define kFarmingDetail     @"farmwork.getfarmworkdetail"
#define kArea              @"farmwork.getplantsarchives"
#define kAgricultrue       @"farmwork.getagriculturalproduct"
#define kOperationContent  @"farmwork.getfarmworktype"
#define kAddFarming        @"farmwork.farmworkadd"
#define kEditFarming       @"farmwork.farmworkedit"
#define kDeleteFarming     @"farmwork.delfarmwork"
#define kPlantIndex        @"landarchives.index"
#define kPlantCrop         @"Landarchives.plantarchives"
#define kHistoryCrop       @"Landarchives.allplantarchives"
#define kCropDetail        @"Landarchives.plantarchivesdetail"
#define kAllCrop           @"Landarchives.productoption"
#define kAddCrop           @"landarchives.plantarchivesadd"

#endif /* URLMacros_h */
