//
//  FontAndColorMacros.h
//  Farming
//
//  Created by 甘世伟 on 2017/8/11.
//  Copyright © 2017年 rongkai. All rights reserved.
//

#ifndef FontAndColorMacros_h
#define FontAndColorMacros_h

#define kNavigationLeftColor UIColorFromHex(0x00C951)
#define kNavigationRightColor UIColorFromHex(0x78D706)
#define kBackgroundColor UIColorFromHex(0xF2F2F2)
#define kGreenColor UIColorFromHex(0x18B018)
#define kWhiteColor UIColorFromHex(0xFFFFFF)
#define kFontBlackColor UIColorFromHex(0x333333)
#define kFontGrayColor UIColorFromHex(0xB3B3B3)

#define kSystemFont(a) [UIFont systemFontOfSize:a]


#define kPlaceHolderImage [UIImage imageNamed:@"placeHolder"]

#endif /* FontAndColorMacros_h */
